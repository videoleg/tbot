class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :currency
      t.integer :type
      t.float :stop
      t.float :start
      t.float :exit
      t.float :close
      t.float :amount
      t.text :message
      t.integer :timestamp
      t.timestamps
    end
  end
end
