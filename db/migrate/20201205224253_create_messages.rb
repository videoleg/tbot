class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      t.string :uid
      t.json :payload
      t.string :text
      t.integer :status

      t.timestamps
    end
  end
end
