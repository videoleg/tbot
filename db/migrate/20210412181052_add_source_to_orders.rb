class AddSourceToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :source, :integer, default: 0
    add_column :orders, :status, :integer, default: 0
  end
end
