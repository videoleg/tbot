class AddNewColumnsToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :risk, :float
    add_column :orders, :setup, :json
    add_column :orders, :image, :string
    add_column :orders, :outcome, :float
    add_column :orders, :outcome_status, :integer, default: 0
  end
end
