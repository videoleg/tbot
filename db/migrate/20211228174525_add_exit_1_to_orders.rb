class AddExit1ToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :exit_1, :float
    add_column :orders, :exit_2, :float
  end
end
