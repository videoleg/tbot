class CreateWaves < ActiveRecord::Migration[6.0]
  def change
    create_table :waves do |t|
      t.references :message, index: true
      t.references :order, index: true
      t.text :text
      t.text :note
      t.integer :status
      t.timestamps
    end
  end
end
