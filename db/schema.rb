# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_28_174525) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "messages", force: :cascade do |t|
    t.string "uid"
    t.json "payload"
    t.string "text"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "source"
  end

  create_table "orders", force: :cascade do |t|
    t.string "currency"
    t.integer "type"
    t.float "stop"
    t.float "start"
    t.float "exit"
    t.float "close"
    t.float "amount"
    t.text "message"
    t.integer "timestamp"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "source", default: 0
    t.integer "status", default: 0
    t.text "comment"
    t.float "risk"
    t.json "setup"
    t.string "image"
    t.float "outcome"
    t.integer "outcome_status", default: 0
    t.float "exit_1"
    t.float "exit_2"
  end

  create_table "waves", force: :cascade do |t|
    t.bigint "message_id"
    t.bigint "order_id"
    t.text "text"
    t.text "note"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["message_id"], name: "index_waves_on_message_id"
    t.index ["order_id"], name: "index_waves_on_order_id"
  end

end
