#!/bin/sh

#bundle exec rails s -b 0.0.0.0 -p 4000

set -e

if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi

bundle check || bundle install
bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake assets:precompile
ruby daemon.rb stop
ruby daemon.rb start
bundle exec unicorn -c config/unicorn.rb
