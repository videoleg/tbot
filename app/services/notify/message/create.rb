class Notify::Message::Create < Notify::Base
  def initialize(message, source)
    @message = message
    @source = source
  end

  def call
    notify(payload, chat)
  end

  private

  def payload
    [
      root,
      body
    ].join(SEPARATOR)
  end

  def root
    "<b>Message from KATY</b> #M_#{@message.id} (#{@source})"
  end

  def body
    @message.text
  end

  def chat
    ENV['BOT_CHAT_KATY']
  end
end
