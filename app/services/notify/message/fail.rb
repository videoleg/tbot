class Notify::Message::Fail < Notify::Base
  def initialize(original, signal, note = 'unknown error')
    @original = original
    @signal = signal
    @note = note
  end

  def call
    notify(message, chat)
  end

  private

  def message
    [
      root,
      body,
      reason
    ].join(SEPARATOR)
  end

  def root
    '<b>Unrecognized signal</b>'
  end

  def body
    [
      "original: #{@original}",
      "signal: #{@signal}"
    ].join(SEPARATOR)
  end

  def reason
    "reason: #{@note}"
  end

  def chat
    ENV['BOT_CHAT_KATY']
  end
end
