class Notify::Order::Cancel < Notify::Order::Create
  private

  def changes
    {}
  end

  def event
    'canceled'
  end
end
