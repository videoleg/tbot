class Notify::Order::Base < Notify::Base
  include Callable

  CHANGES = %w[
    stop
    start
    exit
    exit_1
    exit_2
    close
    risk
    setup
    outcome
    outcome_status
    image
    comment
  ].freeze

  VALUES_MAP = {
    setup: ->(v) { v&.join(',') }
  }.freeze

  KEYS_MAP = {
    exit: ->(*) { 'TP 1' },
    exit_1: ->(*) { 'TP 2' },
    exit_2: ->(*) { 'TP 3' },
    stop: ->(*) { 'SL' },
    start: ->(*) { 'WHEN' }
  }.freeze

  attr_reader :order

  def initialize(order)
    @order = order
  end

  def call
    notify(message, chat)
  end

  private

  def message
    root_message
  end

  def t(key) # translator
    KEYS_MAP[key.to_sym]&.call || key.to_s.upcase
  end

  def m(key, value) # modificator
    VALUES_MAP[key.to_sym]&.call(value) || value
  end

  def root_message
    [link, event, tag, anchor].compact.join(' ')
  end

  def tag
    "##{order.source[0]}_#{order.id}".upcase
  end

  def anchor
    if order.waves.any?
      order.waves.map do |wave|
        "#M_#{wave.message_id}"
      end.join(' ')
    end
  end

  def event
    ''
  end

  def link
    "<a href='#{href}'><b>#{"#{order.type} #{order.currency.upcase}"}</b></a>"
  end

  def href
    "#{ENV['HOST']}/#/orders/#{order.id}"
  end

  def chat
    if order.artur?
      ENV['BOT_CHAT_ARTUR']
    else
      ENV['BOT_CHAT_KATY']
    end
  end
end
