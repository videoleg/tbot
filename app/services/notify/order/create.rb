class Notify::Order::Create < Notify::Order::Base
  private

  def event
    'created'
  end

  def changes
    order.attributes.slice(*CHANGES).select { |_, v| v.present? }
  end

  def changes_message
    changes.map do |k, v|
      "#{t(k)}: <code>#{m(k, v)}</code>"
    end.join(SEPARATOR)
  end

  def message
    [root_message, changes_message].join(SEPARATOR)
  end
end
