class Notify::Order::Update < Notify::Order::Create
  attr_reader :changes

  def initialize(order, changes)
    super(order)
    @changes = changes.select do |k, _v|
      CHANGES.include?(k.to_s)
    end
  end

  private

  def event
    'updated'
  end
end
