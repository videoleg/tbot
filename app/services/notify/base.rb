class Notify::Base
  include Callable

  SEPARATOR = "\n".freeze

  def notify(text, chat_id = nil)
    Notify.call(text, chat_id)
  end
end
