# id: message.id,
# chat_id: message.chat_id,
# sender_id: message.sender_user_id,
# datetime: message.date,
# text: begin message.content.text.text rescue nil end,
# original: message.to_hash

class Message::Save
  SOURCES = {
    ENV['TG_SOURCE_1'] => :smooth_katy,
    ENV['TG_SOURCE_2'] => :katy,
    ENV['TG_SOURCE_3'] => :clever_pips
  }.freeze

  class << self
    def call(payload)
      payload = OpenStruct.new(payload)
      return unless payload.text.present?
      return if Message.find_by(uid: payload.id)

      source = SOURCES[payload.chat_id.to_s] || :clever_pips # temp
      return unless source

      message = Message.create!(
        payload: payload.original,
        text: payload.text,
        uid: payload.id,
        source: source
      )

      Message::Parse.call(message, source)
    end
  end
end
