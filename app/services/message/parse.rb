class Message::Parse
  include Callable

  attr_reader :text, :message, :parsed, :source

  def initialize(message, source = :katy)
    @message = message
    @text = message.text
    @source = source
  end

  def call
    parse!
    log!
    process!
  end

  def log!
    Notify::Message::Create.call(message, source)
  end

  def process!
    signals.each do |signal_text|
      wave = Wave.create!(
        message: message,
        text: signal_text
      )
      Wave::Process.call(wave, source)
    end
  end

  def parse!
    @parsed = @text.dup.downcase
        .gsub(/[^0-9A-Za-z.]/, ' ') # only chars, numbers and dots
        .gsub('at', '')
        .gsub(Message::DIGIT_FIX_REGEX) { |s| " #{s.to_f}" } # replace digits like .111
        .split.join(' ') # remove unnessecary spaces
  end

  def signals
    @signals ||= begin
      signals_count = 0
      items = []

      parsed.split(' ').each do |word|
        signals_count += 1 if Message::SPLIT_WORDS.include?(word)
        items[signals_count] ||= []
        items[signals_count] << word
      end

      items.compact.map {|ar| ar.join(' ') }
    end
  end
end
