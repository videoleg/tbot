class Notify
  include Callable

  attr_reader :text, :chat_id

  def initialize(text, chat_id = nil)
    @text = text
    @chat_id = chat_id
  end

  def call
    return unless chat_id

    $bot.send_message(params)
  end

  def params
    { chat_id: chat_id || default_chat_id, text: text, parse_mode: 'HTML' } # https://core.telegram.org/bots/api#html-style
  end

  def default_chat_id
    ENV['BOT_CHAT_ARTUR']
  end
end
