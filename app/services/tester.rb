class Tester
  MESSAGES = [
    # "Buy 1 unit CADCHF at .6958 Stop at .6928 Exit at .6978",
    # "Close   EURJPY   at   126.17",
    # "Close CADCHF long at .6954",
    # "Sorry for toying around but the short side looks better for CADCHF like CADJPY",
    # "Buy 1 unit USDCAD at 1.2703 Stop at 1.2673 Exit at 1.2723",
    # "Buy 1 unit CADCHF at .6958 Stop at .6928 Exit at .6978",
    # "Sell 1 unit CADCHF at .6952 Stop at .6975 Exit at .6932 Sticking with these trades for now. No new trades just closes from here on",
    # "Buy 1 unit USDCAD at 1.2716 Stop at 1.2682 Exit at 1.2736",
    # "Close CADCHF at .6961",
    # "Close USDCAD at 1.2795",
    # "One last shot :triangular_flag_on_post: Buy 1 unit USDCAD at 1.2712 Stop at 1.2687 Exit at 1.2732",
    # "Close USDCAD at 1.2717",
    # "Close AUDNZD at 1.0660",
    # "Sell 1 unit AUDNZD at 1.0649 Stop at 1.0682 Exit at 1.0629",
    # "Buy 1 unit USDCAD at 1.2733 Stop at 1.2700 Exit at 1.2753",
    # "Sell 1 unit USDCHF at .8840 Stop at .8870 Exit .8820",
    # "Sell 1 unit CHFJPY at 116.75 Stop at 117.07 Exit at 116.55",
    # "Sell 1 unit CADJPY at 81.01 Stop at 81.35 Exit at 80.81",
    # "Sell NZDCHF at .6292 Stop at .6323 Exit 1/2 at .6272, move stop on rest to breakeven Exit rest at .6252",
    # "Sell 1 unit USDCHF at .8829 Stop at .8860 Exit at .8809",
    # "Close AUDCAD at .9706",
    # "Buy 1 unit AUDUSD at .7607 Stop at .7574 Exit at .7627",
    # "Close 1/2 NZDCHF at .6291.Reduce NZDCHF to one position",
    # "Sorry Sell 1 more unit NZDCHF at .6292 - still like this trade. Lets keep it on with original levels",
    # "Sell 1 unit USDJPY at 103.37 Stop at 103.60 Exit at 103.17",
    # "Close 1 unit NZDCHF at .6295",
    # "Close CADJPY at 81.06",
    # "Sell 1 unit CHFJPY at 116.88 Stop at 117.20 Exit at 116.68",
    # "Close NZDCHF at .6303",
    # "Sell 1 unit USDJPY at 103.37 Stop at 103.60 Exit at 103.17",
    # "Sell 1 unit CHFJPY at 116.88 Stop at 117.20 Exit at 116.68",
    # "Buy 1 unit AUDUSD at .7607 Stop at .7574 Exit at .7627",
    # "Sell 1 unit USDCHF at .8829 Stop at .8860 Exit at .8809",
    # "Close USDJPY at 103.20",
    # "Close USDCHF at .8848",
    # "Sell 1 unit UAUDUSDSDJPY at 103.32 Stop at 103.60 Exit at 103.12",
    # "Close CHFJPY at 116.70",
    # "Buy 1 unit USDCHF at .8859 Stop at .8829 Exit at .8879 Dollar reversal",
    # "Sell 1 unit EURUSD at 1.2231",
    # "Sell 1 unit EURAUD at 1.6093 Stop at 1.6125 Exit at 1.6073",
    # "Close USDJPY at 103.32",
    # "Close EURUSD at 1.2237"
    "Buy 1 unit AUDUSD at.7752 Stop at .7722 Exit at .7772",
    "Close AUDUSD at .,7742"
  ].freeze

  def self.call
    MESSAGES.each do |text|
      Message.create!(text: text)
    end
    Message.find_each do |message|
      Message::Parse.new(message).call
    end
  end
end
