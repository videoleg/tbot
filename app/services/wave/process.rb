class Wave::Process
  include Callable

  attr_reader :wave, :text, :order, :source

  def initialize(wave, source)
    @wave = wave
    @text = wave.text
    @source = source
  end

  def call
    if new_order?
      new_order!
    else
      unrecognized!
    end
  end

  def new_order?
    text.exclude?('pips')
  end

  def new_order!
    result = Order::Operation::Create.call(params: new_order_params)

    if result.success?
      @order = result['model']
      processed!
    else
      failed!("error while order creation: #{errors(result)}")
    end
  end

  def new_order_params
    {
      amount: 1,
      source: source.to_s,
      currency: currency,
      type: text.split(' ')[0],
      start: text.split(' ')[2],
      exit: text.match(/(tp1)( )(#{Message::DIGIT_REGEX})/).try(:[], 0)&.split(' ')&.last,
      exit_1: text.match(/(tp2)( )(#{Message::DIGIT_REGEX})/).try(:[], 0)&.split(' ')&.last,
      exit_2: text.match(/(tp3)( )(#{Message::DIGIT_REGEX})/).try(:[], 0)&.split(' ')&.last,
      stop: text.match(/(sl)( )(#{Message::DIGIT_REGEX})/).try(:[], 0)&.split(' ')&.last,
      message: text,
      wafe_ids: waves
    }
  end

  def processed!
    wave.update!(
      status: :processed
    )
  end

  def waves
    [wave.id]
  end

  def currency
    (text.split(' ') & Currency.all.map(&:downcase)).first.upcase
  end

  def failed!(note = 'unknown error')
    wave.update!(status: :failed, note: note)
    #Notify::Message::Fail.call(wave.message.text, wave.text, note)
  end

  def unrecognized!
    wave.destroy!
  end

  def errors(result)
    result['contract.default'].errors.messages
  end
end
