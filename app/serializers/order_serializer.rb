class OrderSerializer < ApplicationSerializer
  attributes :currency,
             :type,
             :stop,
             :start,
             :exit,
             :exit_1,
             :exit_2,
             :close,
             :amount,
             :message,
             :comment,
             :timestamp,
             :created_at,
             :updated_at,
             :source,
             :status,
             :risk,
             :setup,
             :image,
             :outcome,
             :outcome_status

  attribute :waves do |object|
    object.waves.map do |wave|
      {
        id: wave.id,
        text: wave.text,
        status: wave.status,
        note: wave.note
      }
    end
  end
end
