class MessageSerializer < ApplicationSerializer
  attributes :text, :source

  attribute :timestamp do |object|
    object.created_at.to_i
  end

  attribute :waves do |object|
    object.waves.map do |wave|
      order = wave.order
      {
        text: wave.text,
        status: wave.status,
        note: wave.note,
        timestamp: wave.created_at.to_i,
        order: order ? {
          id: order.id,
          type: order.type,
          amount: order.amount,
          start: order.start,
          currency: order.currency,
          stop: order.stop,
          exit: order.exit
        } : nil
      }
    end
  end
end
