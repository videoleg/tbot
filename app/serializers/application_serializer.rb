class ApplicationSerializer
  include JSONAPI::Serializer

  def initialize(resource, options = {})
    process_options options.merge(params: { result: resource })

    @resource = resource[:model]
  end
end
