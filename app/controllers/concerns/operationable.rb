module Operationable
  def run!(operation_klass, dependency_params = {})
    result = operation_klass.call(params: parameters, current_user: nil, **dependency_params)

    return yield(result) if block_given?

    if result.success?
      render json: serialize(result)
    elsif result['model'].nil?
      raise ActiveRecord::RecordNotFound
    else
      render json: invalid_response(result), status: :unprocessable_entity
    end
  end

  private

  def parameters
    params.permit!.merge \
      (params.dig(:data) || {})
  end

  def serialize(result)
    serializer_class(result).new(result, serializer_options(result))
  end

  def serializer_class(result)
    model_class = result['model'].is_a?(ActiveRecord::Relation) ? result['model'].model : result['model'].class
    (result['serializer'] || [model_class, 'Serializer'].join.constantize)
  end

  def serializer_options(result)
    is_collection = result['model'].is_a?(ActiveRecord::Relation)

    {
      is_collection: is_collection,
      meta: is_collection ? meta(result) : {}
    }
  end

  def meta(result)
    {
      total: result['model'].total_count
    }
  end

  def invalid_response(result)
    errors = result['contract.default']&.errors&.messages.presence || result['errors'].to_h
    {
      errors: errors.map do |key, message|
        error_message(key, message)
      end
    }
  end

  def error_message(key, message)
    {
      status: 422,
      source: key,
      title: 'validation failed',
      detail: message.join(',')
    }
  end
end
