class ApplicationController < ActionController::Base
  before_action :set_gon

  def set_gon
    gon.currencies = CURRENCIES.sort
  end
end
