class OrdersController < ApplicationController
  include Operationable

  http_basic_authenticate_with name: ENV['AUTH_USERNAME'], password: ENV['AUTH_PASSWORD']

  def index
  end

  def export
    result = Base::Operation::Index.call(params: parameters, model: Order,  current_user: nil)
    filename = "Orders #{Time.zone.now.strftime('%H.%M %d.%m.%Y')}"
    records = result[:model].to_a
    render xlsx: Order.to_xlsx(instances: records, sheet_name: filename), filename: filename
  end
end