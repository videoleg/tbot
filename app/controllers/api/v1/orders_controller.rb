module Api
  module V1
    class OrdersController < ::Api::V1::ApplicationController
      def index
        result = Base::Operation::Index.(params: { filter: filter, page: page }, model: Order)
        render json: result['model'].map { |record| serialize(record) }
      end

      private

      def filter
        filters = []

        params.slice(:source, :period).as_json.map do |key, value|
          filters << {
            key: key,
            value: value,
            type: FILTER_TYPE[:eq]
          }
        end

        filters << {
          key: "status",
          value: "active",
          type: FILTER_TYPE[:eq]
        }

        filters
      end

      def page
        {
          number: 1,
          size: 100
        }
      end

      def serialize(order)
        attributes = %w(id timestamp currency type stop start exit amount source exit_1 exit_2)
        order.attributes.slice(*attributes)
      end
    end
  end
end
