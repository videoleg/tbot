module Api
  module V2
    class MessagesController < ::Api::V2::ApplicationController
      def index
        run! Base::Operation::Index, model: Message.includes(:orders, :waves)
      end
    end
  end
end
