module Api
  module V2
    class OrdersController < ::Api::V2::ApplicationController
      def index
        run! Base::Operation::Index, model: Order
      end

      def create
        run! Order::Operation::Create
      end

      def update
        run! Order::Operation::Update
      end

      def close
        run! Order::Operation::Close
      end

      def destroy
        order = Order.find(params[:id])
        order.destroy!
        render json: { ok: :ok }
      end

      def show
        run! Base::Operation::Show, model: Order
      end
    end
  end
end
