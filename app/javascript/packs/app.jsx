import React from 'react';
import ReactDOM from 'react-dom';
import { Root } from './app/Root';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<Root />, document.body.appendChild(document.getElementById('app')));
})
