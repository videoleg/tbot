import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useStore } from '@app/store/RootStore';
import { OrderForm } from '@app/components/OrderForm/OrderForm';
import {
  useHistory,
  generatePath,
} from 'react-router-dom';
import { ROUTES } from '@app/Root';
import {useAlerts} from '@app/components/Alerts';
import {Container} from "@material-ui/core";

export const NewOrder = () => {
  const { orders: ordersStore } = useStore();
  const history = useHistory();
  const alerts = useAlerts();

  const submitHandler = (data) => {
    return ordersStore.create(data).then((order) => {
      alerts.success('saved');
      history.push(generatePath(ROUTES.ORDER, { id: order.id }));
    });
  };

  return <Container>
    <Box mt={2}>
      <Typography variant={'h4'}>
        New order
      </Typography>
    </Box>
    <Box mt={2}>
      <OrderForm onSubmit={submitHandler}/>
    </Box>
  </Container>
}
