import React, {useEffect} from 'react';
import { observer } from 'mobx-react-lite';
import { useParams } from 'react-router-dom';
import {useAlerts} from '@app/components/Alerts';
import {SkeletonTable} from '@app/components/Loaders';
import { OrderForm } from '@app/components/OrderForm/OrderForm';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useStore } from '@app/store/RootStore';
import {List} from '@material-ui/core';
import { Wave } from '@app/components/Wave';
import Button from "@material-ui/core/Button";
import Badge from "@material-ui/core/Badge";
import {OrderToolbar} from "../../components/OrderToolbar";
import Container from "@material-ui/core/Container";

export const EditOrder = observer(() => {
  const { id } = useParams();
  const { order: orderStore } = useStore()

  useEffect(() => {
    orderStore.load(id);
  }, [ id ])

  const order = orderStore.item;
  const loading = orderStore.loading;
  const alerts = useAlerts();

  if (loading || !order) {
    return <SkeletonTable />
  }

  const submitHandler = (values) => {
    orderStore.update(values).then(() => {
      alerts.success('Saved');
    });
  };

  const waves = order.waves;

  return (
    <Container>
      <Box mt={2}>
        <OrderToolbar order={order}/>
      </Box>

      <Box mt={4}>
        <OrderForm order={order} onSubmit={submitHandler}/>
      </Box>
      {
        waves.length ? (
          <>
            <Box mt={6}>
              <Typography variant={'h4'}>
                Signals
              </Typography>
              <List>
                {
                  waves.map(wave => {
                    return <Wave wave={wave}/>
                  })
                }
              </List>
            </Box>
          </>
        ) : (<></>)
      }
    </Container>
  )
});
