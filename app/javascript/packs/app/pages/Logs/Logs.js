import React, { useEffect } from 'react';
import { useStore } from '@app/store/RootStore';
import { observer } from 'mobx-react-lite';
import Typography from '@material-ui/core/Typography';
import {PaginatedTable} from '@app/components/BaseTable';
import {Box, List} from '@material-ui/core';
import { useHistory, generatePath } from 'react-router-dom';
import {useAlerts} from '@app/components/Alerts';
import { Wave } from '@app/components/Wave';
import Container from "@material-ui/core/Container";

export const Logs = observer((props) => {
  const { messages } = useStore();

  useEffect(() => {
    messages.load();
  }, [
    messages.page,
    messages.filter,
    messages.sort
  ])

  const columns = [
    {
      key: 'id',
      label: 'ID',
      render: (message) => {
        return <b>{ message.id }</b>
      }
    },
    {
      key: 'source',
      label: 'source',
      render: (message) => {
        return <Typography variant={'caption'}>
          { message.source }
        </Typography>
      }
    },
    {
      key: 'updated',
      label: 'updated',
      render: (message) => {
        return <Typography variant={'caption'}>
          { message.updated }
        </Typography>
      }
    },
    {
      key: 'text',
      label: 'text',
      style: { minWidth: '120px' },
      render: (message) => {
        return (
          <>
            <Typography variant={'caption'}>
              { message.text }
            </Typography>
            <Box mt={2}>
              <List>
                {
                  message.waves.map(wave => {
                    return <Wave wave={wave}/>
                  })
                }
              </List>
            </Box>
          </>
        )
      }
    },
    // {
    //   key: 'action',
    //   label: '',
    //   render: (message) => {
    //     return (
    //       <Typography noWrap>
    //         <IconButton onClick={() => {
    //           history.push(generatePath(ROUTES.ORDER, { id: message.id }));
    //         }}>
    //           <CreateIcon/>
    //         </IconButton>
    //       </Typography>
    //     )
    //   }
    // }
  ]

  return <Container maxWidth={false}>
    <Box mt={2}>
      <Typography variant={'h4'}>
        Logs
      </Typography>
    </Box>
    <Box mt={2}>
      <PaginatedTable
        tableCols={columns}
        store={messages}
      />
    </Box>
  </Container>
});
