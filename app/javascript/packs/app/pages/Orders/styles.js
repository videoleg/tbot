import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(theme => {
  return createStyles({
    badge: {
      right: '-10px'
    },
    title: {
      display: 'flex',
    },
    h1: {
      flexGrow: 1,
    }
  })
});

