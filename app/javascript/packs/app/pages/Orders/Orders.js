import React, { useEffect, useState } from 'react';
import { useStore } from '@app/store/RootStore';
import { observer } from 'mobx-react-lite';
import Typography from '@material-ui/core/Typography';
import {PaginatedTable} from '@app/components/BaseTable';
import {Box} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';
import { useHistory, generatePath } from 'react-router-dom';
import { ROUTES } from '@app/Root';
import Badge from '@material-ui/core/Badge';
import DeleteIcon from '@material-ui/icons/Delete';
import {useAlerts} from '../../components/Alerts';
import { useStyles } from './styles';
import Container from '@material-ui/core/Container';
import {OrderFilters} from '@app/components/OrderFilters';
import Hidden from '@material-ui/core/Hidden';
import {TapCheckbox} from '@app/components/TapControls/TapCheckbox';
import ImageIcon from '@material-ui/icons/Image';
import {Truncated} from '@app/components/Truncated';
import Tooltip from "@material-ui/core/Tooltip";

export const Orders = observer((props) => {
  const { orders } = useStore();
  const classes = useStyles();

  const [expanded, setExpanded] = useState(true);

  useEffect(() => {
    orders.load();
  }, [
    orders.page,
    orders.filter,
    orders.sort
  ])

  const history = useHistory();
  const alerts = useAlerts();

  const actions = [
    {
      key: 'action',
      label: '',
      render: (order) => {
        return (
          <Typography noWrap>
            {
              order.image ? (
                <IconButton onClick={() => {
                  window.open(order.image)
                }}>
                  <ImageIcon/>
                </IconButton>
              ) : (
                <IconButton disabled>
                  <ImageIcon/>
                </IconButton>
              )
            }
            <IconButton onClick={() => {
              history.push(generatePath(ROUTES.ORDER, { id: order.id }));
            }}>
              <CreateIcon/>
            </IconButton>
            <IconButton onClick={() => {
              if (confirm('sure?')) {
                orders.delete(order.id).then(() => {
                  alerts.success('deleted')
                });
              }
            }}>
              <DeleteIcon/>
            </IconButton>
          </Typography>
        )
      }
    }
  ];

  let columns = [
    {
      key: 'id',
      label: 'ID',
      render: (order) => {
        return <Badge
          classes={{ badge: classes.badge }}
          color={order.isClosed ? 'secondary' : 'primary' }
          badgeContent={order.source[0]}
        >
          <Tooltip title={order.source}>
            <b>{ order.id }</b>
          </Tooltip>
        </Badge>
      }
    },
    {
      key: 'type',
      label: 'type',
      render: (order) => <Typography noWrap>{order.type} <code>{order.currency}</code></Typography>
    },
    {
      key: 'when',
      label: 'when',
      render: (order) => order.start
    },
    {
      key: 'stop',
      label: 'SL',
      render: (order) => order.stop
    },
    {
      key: 'TP',
      label: 'TP',
      render: (order) => order.exit
    },
    {
      key: 'updated',
      label: 'updated',
      render: (order) => {
        return <Typography variant={'caption'} noWrap>
          { order.updated }
        </Typography>
      }
    },
  ];

  if (expanded) {
    columns = [
      ...columns,
      {
        key: 'outcome',
        label: 'outcome',
        render: (order) => {
         return (
           <Badge
             classes={{ badge: classes.badge }}
             color={'default'}
             badgeContent={`${order.outcome}`}
           >
             <b>{ order.outcome_status }</b>
           </Badge>
         )
        }
      },
      {
        key: 'setup',
        label: 'setup',
        render: (order) => <Typography noWrap variant={'caption'}>{order.setups}</Typography>
      },
      {
        key: 'risk',
        label: 'risk, %',
        render: (order) => order.risk
      },
      {
        key: 'comment',
        label: 'comment',
        render: (order) => {
          return <Truncated title={order.comment} variant={'caption'} />
        }
      },
      ...actions
    ];
  } else {
    columns = [
      ...columns,
      ...actions
    ]
  }

  return <Container maxWidth={false}>
    <Box mt={2} className={classes.title}>
      <Box component={'div'} className={classes.h1}>
        <Typography variant={'h4'}>
          Orders
        </Typography>
      </Box>
      <Box component={'div'} className={classes.btn}>
        <TapCheckbox
          color={'primary'}
          type={'switch'}
          label={''}
          value={expanded}
          onChange={setExpanded}
        >
        </TapCheckbox>
      </Box>
    </Box>
    <Box mt={4}>
      <OrderFilters />
    </Box>
    <Box mt={4}>
      <PaginatedTable
        tableCols={columns}
        store={orders}
        rowsPerPageOptions={[10, 25, 50, 100]}
      />
    </Box>
  </Container>
});
