import 'mobx-react-lite/optimizeForReactDom';
import React from 'react';
import { Layout } from '@app/components/Layout';
import {
  HashRouter as Router, Route, Switch,  Redirect
} from 'react-router-dom';
import { NewOrder } from './pages/NewOrder';
import { EditOrder } from './pages/EditOrder';
import { Logs } from './pages/Logs';
import { Orders } from './pages/Orders';
import { configure } from 'mobx';
import { ThemeProvider, StylesProvider } from '@material-ui/core';
import { RootStore, StoreProvider } from '@app/store/RootStore';
import { SnackbarProvider } from 'notistack';

const ROOT_PATH = '/';

export const ROUTES = {
  ROOT: ROOT_PATH ,
  ORDERS: `${ROOT_PATH}orders`,
  NEW_ORDER: `${ROOT_PATH}new_order/`,
  ORDER: `${ROOT_PATH}orders/:id`,
  LOGS: `${ROOT_PATH}logs`
};

configure({
  enforceActions: 'observed'
});

export const Root = () => {
  return (
    <StylesProvider injectFirst>
      <ThemeProvider>
        <StoreProvider rootStore={new RootStore()}>
          <SnackbarProvider>
            <Router>
              <Layout>
                <Switch>
                  <Route exact path={ROUTES.ROOT} component={Orders} />
                  <Route exact path={ROUTES.ORDERS} component={Orders} />
                  <Route exact path={ROUTES.LOGS} component={Logs} />
                  <Route exact path={ROUTES.NEW_ORDER} component={NewOrder} />
                  <Route exact path={ROUTES.ORDER} component={EditOrder} />
                  <Redirect to={ROUTES.ROOT} />
                </Switch>
              </Layout>
            </Router>
          </SnackbarProvider>
        </StoreProvider>
      </ThemeProvider>
    </StylesProvider>
  );
};
