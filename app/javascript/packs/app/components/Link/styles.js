import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(theme => {
  return createStyles({
    link: {
      ...theme.typography.button,
      textDecoration: 'none',
      color: 'inherit',
      verticalAlign: 'sub',
      '&:hover': {
        color: 'inherit',
        opacity: 0.8
      }
    },
  })
});

