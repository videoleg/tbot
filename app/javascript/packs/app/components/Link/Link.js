import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useStyles } from './styles';
import Button from "@material-ui/core/Button";

export const Link = ({...props}) => {
  const classes = useStyles();
  return (
    <div className={classes.frame}>
      <RouterLink className={classes.link} {...props}/>
    </div>
  )
}
