import React, { useCallback } from 'react';
import { noop } from 'lodash-es';
import { Formik } from 'formik';
import { FormikErrors } from '@app/components/FormikControls/FormikErrors';
import { FormikState } from './FormikState';

const processErrors = (error) => {
  if (error && error.response && error.response.data) {
    const result = {};
    error.response.data.errors.forEach(obj => {
      result[obj.source] = obj.detail;
    });
    return result;
  } else {
    return { base: ['server error'] }
  }
}

export const FormikExtend = (props) => {
  const onChange = props.onChange || noop;
  const submit = props.onSubmit;

  const onSubmit = useCallback(
    (values, actions) => {
      if (props.onSubmit) {
        const submitResult = submit(values, actions);

        if (submitResult && submitResult.then) {
          submitResult
            .catch((err) => {
              return actions.setErrors(processErrors(err));
            })
            .finally(() => {
              actions.setSubmitting(false);
            });
        } else {
          actions.setSubmitting(false);
        }
      }
    },
    [props.onSubmit]
  );

  return (
    <Formik {...props} onSubmit={onSubmit}>
      {(formik) => (
        <>
          <FormikErrors />
          <FormikState onChange={onChange} {...formik}>
            {props.children}
          </FormikState>
        </>
      )}
    </Formik>
  );
}
