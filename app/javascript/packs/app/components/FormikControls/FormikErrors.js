import React, { useEffect, useState } from 'react';
import { ErrorContainer } from '@app/components/ErrorContainer';
import { useFormikContext } from 'formik';

export const FormikErrors = () => {
  const formik = useFormikContext();
  const [error, setError] = useState(null);

  useEffect(() => {
    setError(formik.errors);
  }, [formik.errors]);

  return (
    <ErrorContainer
      error={error}
      onClose={() => {
        setError(null);
      }}
    />
  );
};
