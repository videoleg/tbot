import React from 'react';
import { useField } from 'formik';
import { useEventBusContext } from '@app/components/FormikControls/hooks';
import { TapSelect } from '@app/components/TapControls/TapSelect';

// Regular dropdown
export const FormikSelect = ({
  name,
  onChange,
  onClear,
  ...props
}) => {
  const [field, meta, helpers] = useField(name);
  const { setValue } = helpers;
  const eventBus = useEventBusContext();

  const handleChange = (value) => {
    setValue(value);
    onChange && onChange(value);
    eventBus.notify('change', { [field.name]: value });
  };

  const handleClear = () => {
    onClear && onClear();
    eventBus.notify('change', { [field.name]: null });
  };

  return (
    <TapSelect
      {...props}
      value={field.value}
      error={meta.touched && !!meta.error}
      onChange={handleChange}
      onClear={onClear && handleClear}
    />
  );
};
