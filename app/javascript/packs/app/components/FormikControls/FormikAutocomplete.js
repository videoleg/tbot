import React, {
  useCallback
} from 'react';
import { useField } from 'formik';
import { useEventBusContext } from '@app/components/FormikControls/hooks';
import { TapAutocomplete } from '@app/components/TapControls/TapAutocomplete';

export function FormikAutocomplete({
  name,
  options,
  onChange,
  ...props
}) {
  const [field, meta, helpers] = useField(name);
  const { setValue } = helpers;

  options = options || [];

  const eventBus = useEventBusContext();

  const handleValueSet = useCallback((value) => {
    setValue(value);
    eventBus.notify('change', { [field.name]: value });
  }, [setValue, eventBus, field]);

  const handleChange = useCallback((value) => {
    handleValueSet(value);
    onChange && onChange(value);
  }, [onChange, handleValueSet]);

  return (
    <TapAutocomplete
      {...props}
      options={options}
      value={field.value}
      onChange={handleChange}
      error={meta.touched && !!meta.error}
    />
  )
}
