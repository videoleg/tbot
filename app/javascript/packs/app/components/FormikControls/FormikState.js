import React, { useEffect, useState } from 'react';
import { toPairs } from 'lodash-es';
import { EventBusCtxProvider } from './hooks/useEventBusContext';
import { useEventBus } from './hooks/useEventBus';
import { useFormikContext } from 'formik';

export const FormikState  = (props) => {
  const eventBus = useEventBus();
  const { children } = props;
  const { values } = useFormikContext();
  const [state, setState] = useState(null);

  useEffect(() => {
    if (state !== null) {
      const [name] = toPairs(state)[0];
      props.onChange(values, name);
      setState(null);
    }
  }, [state, props.onChange, values]);

  useEffect(() => {
    const unlisten = eventBus.listen('change', (field) => {
      setTimeout(() => {
        setState(field);
      }, 0);
    });
    return () => unlisten();
  }, []);

  return (
    <EventBusCtxProvider value={eventBus}>
      {typeof children === 'function' ? children(props) : children}
    </EventBusCtxProvider>
  );
};
