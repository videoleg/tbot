import React, { useCallback } from 'react';
import { useField } from 'formik';
import { useEventBusContext } from '@app/components/FormikControls/hooks';
import { TapCheckbox } from '@app/components/TapControls/TapCheckbox';

export const FormikCheckbox = ({
  name,
  dataCy,
  onChange,
  ...props
}) => {
  const [field, meta, helpers] = useField(name);
  const eventBus = useEventBusContext();
  const { setValue } = helpers;

  const handleChange = useCallback((value) => {
    setValue(value);
    onChange && onChange(value);
    eventBus.notify('change', { [field.name]: value });
  }, [onChange]);

  const materialProps = {
    checked: meta.value,
    onChange: handleChange,
    inputProps: { 'data-cy': dataCy },
    ...props
  };

  return (
    <TapCheckbox
      {...materialProps}
      {...props}
    />
  );
};
