import { createContext, useContext } from 'react';

const EventBusCtx = createContext({});

export const EventBusCtxProvider = EventBusCtx.Provider;

export const useEventBusContext = () => {
  return useContext(EventBusCtx);
};
