import { useMemo } from 'react';
import { EventBus } from './EventBus';

export const useEventBus = () => useMemo(() => new EventBus(), []);
