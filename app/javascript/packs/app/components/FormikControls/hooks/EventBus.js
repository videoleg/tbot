export class EventBus {
  listeners = {};

  listen(event, cb) {
    this.listeners[event] = this.listeners[event] || [];
    this.listeners[event].push(cb);
    return () =>
      (this.listeners[event] = this.listeners[event].filter(c => c !== cb));
  }
  notify(event, value) {
    if (!this.listeners[event]) {
      return;
    }
    for (const listener of this.listeners[event]) {
      listener(value);
    }
  }
}
