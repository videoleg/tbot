import React, { useCallback, useEffect, useState } from 'react';
import { useField } from 'formik';
import { useEventBusContext } from './hooks';
import { TapTextField } from '@app/components/TapControls/TapTextField';

// Regular text field
export const FormikTextField = ({
  name,
  label,
  debounce = false,
  tooltip = false,
  onChange,
  onBlur,
  ...props
}) => {
  const [field, meta, helpers] = useField(name);
  const { setValue } = helpers;
  const [textValue, setTextValue] = useState(field.value);

  let timeout;
  const eventBus = useEventBusContext();

  useEffect(() => {
    setTextValue(field.value);
  }, [field.value]);

  const handleChange = useCallback((value) => {
    setTextValue(value);

    if (debounce) {
      if (timeout) clearTimeout(timeout);
      timeout = setTimeout(() => {
        setValue(value);
        eventBus.notify('change', { [field.name]: value });
      }, 750);
    } else {
      setValue(value);
      eventBus.notify('change', { [field.name]: value });
    }
    onChange && onChange(value);
  }, []);

  const handleOnBlur = (value) => {
    field.onBlur(value);
    onBlur && onBlur(field.value);
  };

  return (
    <TapTextField
      {...props}
      onChange={handleChange}
      value={textValue}
      id={field.name}
      label={label}
      error={meta.touched && !!meta.error}
      onBlur={handleOnBlur}
    />
  );
};
