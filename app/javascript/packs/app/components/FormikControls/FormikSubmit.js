import React from 'react';
import { ButtonProps } from '@material-ui/core/Button';
import { useFormikContext } from 'formik';
import { LoadingButton } from '@app/components/LoadingButton';

export const FormikSubmit = ({
  children,
  color = 'primary',
  variant = 'contained',
  type = 'submit',
  onClick,
  ...props
}) => {
  const formik = useFormikContext();

  return (
    <LoadingButton
      {...props}
      color={color}
      type={type}
      variant={variant}
      disabled={!formik.isValid || props.disabled}
      loading={formik.isSubmitting}
      onClick={onClick ? onClick : formik.submitForm}
    >
      {children}
    </LoadingButton>
  );
};
