import React from 'react';
import { useSnackbar } from 'notistack';

const autoHideDuration = 2000;

const defaultAnchorOrigin = {
  vertical: 'top',
  horizontal: 'right'
};

export const useAlerts = () => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const open = (
    msg,
    options = { variant: 'success' }
  ) => {
    enqueueSnackbar(msg, {
      ...options,
      anchorOrigin: options.anchorOrigin || defaultAnchorOrigin,
      autoHideDuration
    });
  };

  const success = (msg) => {
    open(msg, { variant: 'success' });
  };

  const warning = (msg) => {
    open(msg, { variant: 'warning' });
  };

  const error = (msg) => {
    open(msg, { variant: 'error' });
  };

  return {
    success,
    warning,
    error,
    open
  };
};
