import { createStyles, makeStyles } from "@material-ui/core";
import { colors } from '@app/shared';

export const useClearIndicatorStyles = makeStyles({
  closeIcon: {
    fontSize: '1.7rem'
  },
});

export const useCustomStyles = (size, error, isCheckboxes) => {
  return {
    menuPortal: (base) => ({ ...base, zIndex: 9999 }),
    menu: (provided) => ({ ...provided, zIndex: 99 }),
    option: (provided, { isFocused, isDisabled }) => {
      let value = {
        ...provided,
        color: colors.neutral1,
        backgroundColor: 'transparent'
      }
      if (isFocused) {
        value.backgroundColor = isFocused ? colors.blue5 : 'transparent';
      }
      if (isCheckboxes) {
        value.padding = '0 12px';
      }
      if (isDisabled) {
        value.opacity = 0.6;
      }
      return value;
    },
    control: (provided, { menuIsOpen }) => {
      let value = {
        ...provided,
        minHeight: size == 'small' ? '38px' : '56px',
        borderColor: colors.neutral5,
        '&:hover': {
          borderColor: menuIsOpen ? colors.blue2 : colors.neutral1
        },
      };
      if (menuIsOpen) {
        value.borderColor = `${colors.blue1} !important`;
        value.boxShadow = `0 0 0 1px ${colors.blue1}`;
      } else {
        value.boxShadow = 'none';
      }
      if (error && !menuIsOpen) {
        value.borderColor = `${colors.red1} !important`;
      }
      return value;
    },
    indicatorSeparator: (provided) => ({ ...provided, display: 'none' }),
    valueContainer: (provided) => ({
      ...provided,
      paddingTop: '5px',
      paddingBottom: '1px',
      minHeight: '20px'
    }),

    dropdownIndicator: (provided) => ({ ...provided, paddingLeft: 0 }),
    placeholder: (provided) => ({ ...provided, color: colors.neutral2 }),
    indicatorsContainer: (provided) => ({
      ...provided,
      paddingRight: '0.9rem',
      '& [class$=indicatorContainer]': {
        padding: 0,
        color: colors.neutral2,
        '.MuiSvgIcon-root': {
          transition: 'color 0.1s',
          '&:hover': {
            color: colors.neutral1
          }
        }
      }
    })
  }
}

export const useAutocompleteStyles = makeStyles({
  selectWrapper: {
    position: 'relative',
  },
  selectWrapperDisabled: {
    opacity: 0.6
  },
  optionsPlaceholder: {
    paddingRight: '3px',
    flex: 1,
    position: 'relative',
    top: '-1px',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  customMenu: {
    borderBottom: `1px solid ${colors.neutral5}`,
    padding: '4px 12px'
  },
  baseMenu: {
    padding: '8px 12px'
  },
  baseMenuNoPadding: {
    padding: 0
  },
  inputLabel: {
    position: 'absolute',
    top: '-9px',
    zIndex: 1,
    left: '10px',
    background: 'white',
    padding: '0px 4px',
    width: 'fit-content',
    maxWidth: 'none'
  },
  inputLabelError: {
    color: `${colors.red1} !important`
  },
  inputLabelOpened: {
    color: `${colors.blue2} !important`
  },
  chip: {
    margin: '0.1rem',
    marginBottom: '0.2rem',
  },
  textChip: {
    lineHeight: '1.4em',
    marginBottom: '0.2rem'
  },
  textChipPostfix: {
    marginRight: '3px'
  },
  customInput: {
    marginTop: '0.5rem'
  },
  customMenuButtonsContainer: {
    paddingTop: '1rem',
    marginBottom: '0.1rem'
  },
  customMenuButtonContainer: {}
});

export const useSelectStyles = ({ popperWidth }) =>
  makeStyles(() =>
    createStyles({
      popper: {
        width: popperWidth,
        zIndex: 1400,
        backgroundColor: '#ffffff',
        boxShadow: '0 -0.2rem  0.5rem rgba(27,31,35,.15)',
        overflow: 'auto',
        maxHeight: '30rem',
        borderRadius: '4px',
        top: '6px !important',
        border: `1px solid ${colors.neutral5}`
      },
      textField: {
        width: '100%',
        color: colors.neutral2,
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        paddingRight: '10px',
        '& input': {
          color: colors.neutral1
        },
        borderColor:  colors.neutral5,
      },
      textFieldEmpty: {
        '& input': {
          color: colors.neutral2,
        }
      },
      onClearIcon: {
        cursor: 'pointer',
        fontSize: '1.7rem'
      },
      disablePortal: {
        position: 'absolute'
      }
    })
  );

export const useRadioStyles = makeStyles({
  radioGroup: {
    flexWrap: 'nowrap'
  },
  formControlLabel: {
    marginBottom: 0,
    marginRight: 0,
    padding: '.5rem 1.5rem',
    '& .MuiButtonBase-root': {
      marginRight: 0
    }
  },
  errorWrapper: {
    '& .MuiButtonBase-root': {
      borderColor: colors.red1
    }
  }
});

export const useTextFieldStyles = makeStyles({
  label: {
    background: '#fff',
    paddingRight: '0.6rem'
  },
  input: {
    '&::placeholder': {
      color: colors.neutral2
    }
  }
});
