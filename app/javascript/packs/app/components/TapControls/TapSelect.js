import React, { useState, useCallback, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import CloseIcon from '@material-ui/icons/Close';
import {
  InputAdornment,
  Popper,
  Typography
} from '@material-ui/core';
import cx from 'classnames';
import { useSelectStyles } from './styles';

function DisablePortal(props) {
  const { anchorEl, open, disablePortal, style, ...other } = props;
  return <div {...other} style={style} />;
}
// Regular dropdown
export const TapSelect = ({
  value,
  options,
  label,
  error,
  helperText,
  size = 'medium',
  disabled,
  onChange,
  dataCy,
  onClear,
  clearable,
  autoComplete = 'new-password',
  disablePortal = false,
  popperStyle = {},
  wrapperStyle = {},
  ...props
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [fieldValue, setFieldValue] = useState(value);
  const PortalComponent = disablePortal ? DisablePortal : Popper;

  useEffect(() => {
    setFieldValue(value);
  }, [value]);

  const classes = useSelectStyles({
    popperWidth: anchorEl?.clientWidth
      // anchorEl?.clientWidth && anchorEl?.clientWidth < 200
      //   ? 200
      //   : anchorEl?.clientWidth
  })();

  const handleClick = useCallback((event) => {
    if (disabled) { return }
    setAnchorEl(event.currentTarget);
  }, []);

  const handleChange = (value) => {
    setFieldValue(value);
    onChange && onChange(value);
  };

  const handleClear = (event) => {
    handleChange(null);
    onClear && onClear();
    event.stopPropagation();
  }

  const handleClose = useCallback(e => {
    if (e?.relatedTarget?.id !== 'menu-item-id') setAnchorEl(null);
  },[anchorEl]);

  const open = !!anchorEl;
  const selectedLabel = options.find((o) => o.value === fieldValue)?.label;

  return (
    <div style={wrapperStyle}>
      <TextField
        disabled={disabled}
        onBlur={handleClose}
        variant='outlined'
        label={label}
        helperText={(typeof error == 'string' && error) || helperText}
        value={selectedLabel ? selectedLabel : label}
        error={!!error}
        onClick={handleClick}
        fullWidth
        size={size}
        InputLabelProps={{ 'data-cy': dataCy }}
        inputProps={{
          autoComplete: autoComplete,
          'data-cy': dataCy
        }}
        InputProps={{
          className: `${classes.textField} ${!selectedLabel && classes.textFieldEmpty}`,
          endAdornment: (
            <InputAdornment position='end'>
              {((onClear || clearable) && fieldValue) && (
                <CloseIcon
                  onClick={(e) => {
                    handleClear(e);
                  }}
                  className={classes.onClearIcon}
                />
              )}
              {open ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
            </InputAdornment>
          )
        }}
      />
      {open && (
        <PortalComponent
          placement='bottom-start'
          disablePortal={disablePortal}
          open={open}
          anchorEl={anchorEl}
          className={cx(classes.popper, {
            [classes.disablePortal]: disablePortal
          })}
          style={popperStyle}
        >
          {options.map((option, index) => (
            <MenuItem
              id='menu-item-id'
              key={index}
              onClick={(e) => {
                handleChange(option.value);
                handleClose(e);
              }}
              value={option.value}
            >
              <Typography>
                {option.label}
              </Typography>
            </MenuItem>
          ))}
        </PortalComponent>
      )}
    </div>
  );
};
