import React, { useCallback, useEffect, useState } from 'react';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Switch from '@material-ui/core/Switch';

// Regular checkbox
export const TapCheckbox = ({
  value,
  type,
  label,
  reverse,
  disabled,
  className,
  onChange,
  ...props
}) => {
  const [checked, setChecked] = useState(value);

  useEffect(() => {
    setChecked(value);
  }, [value]);

  const handleChange = useCallback((event) => {
    const checked = event.target.checked;
    const value = reverse ? !checked : checked;
    setChecked(value);
    onChange && onChange(value);
  }, [onChange, setChecked]);

  const materialProps = {
    checked: checked,
    onChange: handleChange,
    ...props,
  };

  return (
    <FormControl component='fieldset'>
      <FormControlLabel
        className={className}
        control={
          type === 'switch' ? (
            <Switch {...materialProps} />
          ) : (
            <Checkbox {...materialProps} />
          )
        }
        label={label}
      />
    </FormControl>
  );
};
