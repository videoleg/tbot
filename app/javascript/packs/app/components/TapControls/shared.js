import React from 'react';
import { Chip, InputLabel, Tooltip, Typography } from '@material-ui/core';
import {  components } from 'react-select';
import DropDownIcon from '@material-ui/icons/ArrowDropDown';
import CloseIcon from '@material-ui/icons/Close';
import { useAutocompleteStyles, useClearIndicatorStyles } from './styles';

export const Label = ({ isOpened, label, isShowed, error }) => {
  const classes = useAutocompleteStyles();
  if (!isShowed) return null;
  return (
    <InputLabel
      className={`
        ${classes.inputLabel} 
        ${error && !isOpened && classes.inputLabelError}
        ${isOpened && classes.inputLabelOpened}
      `}
      shrink={true}
    >
      {label}
    </InputLabel>
  )
}

export const MultiValueContainer = (props) => {
  const { data, selectProps } = props;
  const onDelete = (event) => {
    event.stopPropagation();
    event.preventDefault();
    selectProps.onChange(selectProps.value.filter((el) => el.value != data.value), { action: 'select-option'});
  }
  const classes = useAutocompleteStyles();
  return (
    <Tooltip title={data.label}>
      <Chip
        className={classes.chip}
        onMouseDown={(event) => {
          event.stopPropagation();
        }}
        size={props.chipSize}
        variant='outlined'
        onDelete={onDelete}
        label={data.label}
      />
    </Tooltip>
  );
}

export const ClearIndicator = (props) => {
  const classes = useClearIndicatorStyles();
  return (
    <components.ClearIndicator { ...props }>
      <CloseIcon fontSize='small' className={classes.closeIcon}/>
    </components.ClearIndicator>
  );
}

export const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator { ...props }>
      <DropDownIcon />
    </components.DropdownIndicator>
  )
}

export const Menu = (props) => {
  return (
    <components.Menu {...props}>
      <Typography component='span'>
        {props.children}
      </Typography>
    </components.Menu>
  )
}
