import { Typography } from '@material-ui/core';
import React, { useEffect, useState, useCallback, useMemo } from 'react';
import Select from 'react-select';
import AsyncSelect from 'react-select/async';
import CreatableSelect from 'react-select/creatable';
import AsyncCreatableSelect from 'react-select/async-creatable';
import { compact } from 'lodash-es';
import { useAutocompleteStyles } from './styles';
import { DropdownIndicator, ClearIndicator, MultiValueContainer, Menu, Label} from './shared';
import { useCustomStyles} from './styles';

export function TapAutocomplete({
  label,
  options,
  value,
  clearable,
  onChange,
  multiple,
  creatable,
  async,
  error,
  getOptionDisabled,
  selectId = (v) => v.value,
  selectLabel = (v) => v.label,
  fetchOptions,
  chipSize = 'medium',
  helperText,
  size = 'medium',
  isPortalMenu,
  disabled
}) {

  const [fieldValue, setFieldValue] = useState();
  const [loading, setLoading] = useState(false);
  const classes = useAutocompleteStyles();
  const [open, setOpen] = useState(false);

  const populateValue = useCallback((value, options) => {
    if (!options) return undefined;
    let found = options.find((opt) => selectId(opt) == value);
    if (found) return found;
    if (creatable && (value || value.length)) return {value, label: value};
  }, [selectId]);

  const parseValue = useCallback((value) => {
    if (!value && value !== 0) return undefined;
    if (multiple) {
      return compact(value.map((el) => populateValue(el, options)));
    } else {
      return populateValue(value, options);
    }
  }, [populateValue, options]);

  useEffect(() => {
    if (value || value === 0) {
      if (async) {
        if (typeof value == 'string') {
          loadOptions(value, undefined, true);
        } else {
          console.error(new Error('Temporary async and multiple at once usage is not available. Waiting BE'))
        }
      } else {
        setFieldValue(parseValue(value));
      }
    } else {
      setFieldValue(null);
    }
  }, [value]);

  const handleChange = useCallback((changed) => {
    let val = null;
    if (changed) {
      if (Array.isArray(changed)) {
        val = changed.map(selectId);
      } else {
        val = selectId(changed);
      }
    }
    onChange && onChange(val);
    setFieldValue(changed);
  }, [setFieldValue, onChange, selectId]);

  const loadOptions = (search, callback, population) => {
    setLoading(true);
    fetchOptions && fetchOptions(search).then(res => {
      callback && callback(res.data);
      if (population) {
        setFieldValue(populateValue(value, res.data));
      }
    }).catch(err => {
      console.log(err);
    }).finally(() => {
      setLoading(false);
    });
  }

  const hasValue = useMemo(() => {
    if (Array.isArray(fieldValue)) {
      return fieldValue?.length > 0;
    } else {
      return !!fieldValue;
    }
  }, [fieldValue]);

  const sharedOptions = {
    styles: useCustomStyles(size, !!error),
    value: fieldValue,
    placeholder: label,
    onChange: handleChange,
    menuPortalTarget: isPortalMenu ? document.body : undefined,
    isMulti: multiple,
    isClearable: clearable,
    onMenuOpen: () => setOpen(true),
    onMenuClose: () => setOpen(false),
    getOptionValue: selectId,
    getOptionLabel: selectLabel,
    isOptionDisabled: getOptionDisabled,
    isDisabled: disabled,
    components: {
      DropdownIndicator: DropdownIndicator,
      ClearIndicator: ClearIndicator,
      MultiValueContainer: (props) => <MultiValueContainer {...props} chipSize={chipSize}/>,
      Menu: Menu
    }
  }

  const asyncOptions = {
    defaultOptions: true,
    cacheOptions: true,
    loadOptions,
    isLoading: loading
  }

  const helperTextOrError = (typeof error == 'string' && error) || helperText;

  let component = <Select
    {...sharedOptions}
    options={options}
  />;
  if (async) component = <AsyncSelect
    {...sharedOptions}
    {...asyncOptions}
  />;
  if (creatable) component = <CreatableSelect
    {...sharedOptions}
    options={options}
  />;
  if (async && creatable) component = <AsyncCreatableSelect
    {...sharedOptions}
    {...asyncOptions}
  />;

  return (
    <div className={`${classes.selectWrapper} ${disabled && classes.selectWrapperDisabled}`}>
      <Typography component='div'>
        <Label isOpened={open} label={label} isShowed={hasValue} error={!!error}/>
        {component}
        {helperTextOrError && <Typography variant={'caption'} className={`${!!error ? 'Mui-error' : ''} MuiFormHelperText-root MuiFormHelperText-contained`}>{helperTextOrError}</Typography>}
      </Typography>
    </div>
  )
}
