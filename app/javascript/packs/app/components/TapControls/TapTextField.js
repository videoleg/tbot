import React, { useCallback, useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import { useTextFieldStyles } from './styles';

// Regular text field
export const TapTextField = ({
  value = '',
  label,
  password,
  helperText,
  type = 'text',
  size = 'medium',
  debounce = false,
  disabled = false,
  tooltip = false,
  autoComplete = 'off',
  onKeyPress,
  min,
  max,
  step,
  dataCy,
  onBlur,
  rows,
  error,
  onChange,
  ...props
}) => {
  const [fieldValue, setFieldValue] = useState(value || '');
  const classes = useTextFieldStyles();

  useEffect(() => {
    setFieldValue(value);
  }, [value]);

  const handleChange = useCallback((e) => {
    const val = e.target.value;
    setFieldValue(val);
    onChange && onChange(val);
  }, [onChange]);

  const handleOnBlur = useCallback((ev) => {
    onBlur && onBlur(ev);
  }, [onBlur, fieldValue]);

  return (
    <Tooltip
      title={tooltip ? label : ''}
      placement='top'
    >
      <TextField
        {...props}
        onChange={handleChange}
        value={fieldValue}
        variant='outlined'
        autoComplete={autoComplete}
        type={password? 'password' : type}
        disabled={disabled}
        label={label}
        helperText={(typeof error == 'string' && error) || helperText}
        size={size}
        rows={rows}
        error={!!error}
        fullWidth
        InputProps={{
          classes: {
            input: classes.input
          }
        }}
        InputLabelProps={{
          classes: {
            root: classes.label
          }
        }}
        inputProps={{
          'data-cy': dataCy,
          min,
          max,
          step,
          onKeyPress,
          onBlur: handleOnBlur
        }}
      />
    </Tooltip>
  );
};
