import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 0.05,
  },
  status: {
    flexGrow: 0.05,
  },
  timestamp: {
    flexGrow: 0.9,
  },
}));
