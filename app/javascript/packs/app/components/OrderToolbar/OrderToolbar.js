import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import { useStyles } from './styles';
import {useStore} from '@app/store/RootStore';
import {useAlerts} from '@app/components/Alerts';
import { AppBar, Toolbar, Chip } from '@material-ui/core';


export const OrderToolbar = ({ order }) => {
  const { order: orderStore } = useStore();
  const alerts = useAlerts();
  const classes = useStyles();


  const onCancel = () => {
    if (confirm('Sure?')) {
      orderStore.cancel().then(() => {
        alerts.warning('Order canceled')
      })
    }
  }

  return (
    <>
      <div className={classes.root}>
        <AppBar color={'default'} position='static'>
          <Toolbar>
            <Typography variant='h5' className={classes.title}>
              Order {order.id}
            </Typography>
            <Typography variant={'body1'} className={classes.status}>
              <Chip size={'small'} label={order.status} color={order.isClosed ? 'secondary': 'primary'}/>
            </Typography>
            <Typography variant={'caption'} className={classes.timestamp}>
              { order.updated }
            </Typography>
            {
              !order.isClosed && (
                <Button variant={'contained'} onClick={onCancel} color={'secondary'}>
                  Cancel
                </Button>
              )
            }
          </Toolbar>
        </AppBar>
      </div>
    </>
  )
}
