import React, { useCallback } from 'react';
import Box from '@material-ui/core/Box';
import CloseIcon from '@material-ui/icons/Close';
import { useStyle } from '@app/components/ErrorContainer/styles';
import { isEmpty } from 'lodash-es';

export const ErrorContainer = ({
  error,
  onClose,
  closeable = true
}) => {
  const classes = useStyle();

  const formatErrorResponse = useCallback((err) => {
    if ((err).errors) { // IError
      return formatErrorResponse((err).errors);
    } else { // IKeyValue
      return Object.keys(err).map((key) => {
        let message = (err)[key];

        if (Array.isArray((err)[key])) {
          message = (err)[key].join(', ')
        }

        return (
          <p className={classes.ErrorContainer_item} key={key}>
            {`${key}: ${JSON.stringify(message)}`}
          </p>
        )
      });
    }
  }, [error]);

  if (isEmpty(error) || !error) {
    return null;
  }

  let errorContent;

  if (typeof error === 'string') {
    errorContent = <div>
      <p className={classes.ErrorContainer_item}>
        {error}
      </p>
    </div>
  } else {
    const errorItems = formatErrorResponse(error);
    errorContent = <div>{errorItems}</div>;
  }

  return (
    <Box display='flex' className={classes.ErrorContainer}>
      {errorContent}
      {closeable && (
        <button className={classes.ErrorContainer_close} onClick={onClose}>
          {<CloseIcon className={classes.ErrorContainer_close__icon} />}
        </button>
      )}
    </Box>
  );
};
