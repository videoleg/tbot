import { createStyles, makeStyles } from '@material-ui/core/styles';

const backgroundColor = '#fef4f6';
const color = '#f0506e';

export const useStyle = makeStyles((theme) =>
  createStyles({
    ErrorContainer: {
      marginBottom: '2rem',
      padding: '1rem',
      color: color,
      background: backgroundColor,
      borderRadius: '3px'
    },
    ErrorContainer_item: {
      marginBottom: '0.5rem',
      color: color,
      fontSize: '1rem',
      marginTop: '0.5rem',
      fontFamily: '"Source Sans Pro", sans-serif',
      '&:last-child': {
        marginBottom: 0,
      }
    },
    ErrorContainer_close: {
      marginLeft: 'auto',
      background: 'transparent',
      border: 'none',
      paddingRight: 0,
      outline: 'none',
      cursor : 'pointer'
    },
    ErrorContainer_close__icon: {
      color: color,
      fontSize: '2rem',
      opacity: 0.8,
      '&:hover': {
        opacity: 1
      }
    }
  })
);
