import React from 'react';
import { observer } from 'mobx-react-lite';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TableRow from '@material-ui/core/TableRow';
import TableCell, { Size } from '@material-ui/core/TableCell';
import Hidden from '@material-ui/core/Hidden';

export const BaseTable = observer(
  ({
     tableCols,
     rows,
     onRowClick,
     children,
     pagination,
   }) => {
    return (
      <>
        <TableContainer>
          <Table aria-label='table'>
            <TableHead>
              <TableRow>
                {tableCols.map(colItem => {
                  return (
                    <Hidden mdDown={colItem.hidden}>
                      <TableCell
                        variant='head'
                        size={colItem.size}
                        key={colItem.key}
                      >
                        {colItem.label}
                        {colItem.filter ? colItem.filter : ''}
                      </TableCell>
                    </Hidden>
                  )
                })}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.length > 0 ? (
                rows.map((row, index) => (
                  <TableRow
                    key={index}
                    onClick={() => onRowClick && onRowClick(row)}
                  >
                    {tableCols.map(colItem => {
                      return (
                        <Hidden mdDown={colItem.hidden}>
                          <TableCell
                            style={colItem.style || {}}
                            variant='body'
                            size={colItem.size}
                            key={`${index}_${colItem.key}`}
                            align={colItem.align}
                          >
                            {colItem.render
                              ? colItem.render(row, index)
                              : row[colItem.key]}
                          </TableCell>
                        </Hidden>
                      )
                    })}
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell>No data</TableCell>
                </TableRow>
              )}
            </TableBody>
            {pagination && (
              <TableFooter>
                <TableRow>{pagination}</TableRow>
              </TableFooter>
            )}
          </Table>
          {children}
        </TableContainer>
      </>
    );
  }
);
