import React from 'react';
import { BaseTable } from './BaseTable';
import { Pagination } from '@app/components/Pagination';
import { observer } from 'mobx-react-lite';
import {SkeletonTable} from "@app/components/Loaders";

export const PaginatedTable = observer(({
  store,
  rowsPerPageOptions,
  ...props
}) => {
    if (store.loading) {
      return <SkeletonTable />
    }

    return (
      <BaseTable
        {...props}
        rows={store.all}
        pagination={
          <Pagination
            count={store.total}
            rowsPerPage={store.page.size}
            rowsPerPageOptions={rowsPerPageOptions}
            onChangeRowsPerPage={(
              event
            ) => {
              store.setPageSize(event.target.value);
            }}
            page={store.page.number}
            onChangePage={(_, p) => store.setPage(p)}
          />
        }
      />
    );
  }
);
