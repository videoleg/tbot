import React, { useCallback } from 'react';
import TablePagination from '@material-ui/core/TablePagination';
import { PaginationActions } from './PaginationActions';
import { useStyles } from './styles';

export const Pagination = (props) => {
  const page = props.page - 1;
  const classes = useStyles();
  const onChangePage = useCallback((el, newPage) => {
    props.onChangePage && props.onChangePage(el, newPage + 1);
  }, []);
  return (
    <TablePagination
      {...props}
      classes={{
        spacer: classes.spacer
      }}
      style={{ width: '10px' }}
      page={page}
      onChangePage={onChangePage}
      rowsPerPageOptions={props.rowsPerPageOptions || []}
      ActionsComponent={PaginationActions}
    />
  );
};
