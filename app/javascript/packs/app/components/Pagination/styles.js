import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(theme => {
  return createStyles({
    root: {
      marginLeft: '2rem',
      flexShrink: 0
    },
    spacer: {
      flex: 0
    }
  })
});

