import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

export const LoadingButton = ({
  children,
  loading,
  disabled,
  variant = 'contained',
  color = 'primary',
  type= 'submit',
  ...props
}) => {
  return (
    <Button {...props} color={color} variant={variant} type={type} disabled={disabled || loading}>
      {loading ? <CircularProgress color={'inherit'} size={24} /> : children}
    </Button>
  );
};
