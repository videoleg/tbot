import { makeStyles, createStyles } from '@material-ui/core';

export const useStyles = makeStyles(theme => {
  return createStyles({
    root: {
      boxShadow: 'none',
    },
    summary: {
      padding: 0,
    }
  })
});
