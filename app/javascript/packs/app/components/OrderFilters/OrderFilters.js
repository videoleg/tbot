import React from 'react';
import Grid from "@material-ui/core/Grid";
import {TapTextField} from "../TapControls/TapTextField";
import {TapSelect} from "../TapControls/TapSelect";
import Hidden from "@material-ui/core/Hidden";
import {observer} from "mobx-react-lite";
import { useStore } from "@app/store/RootStore";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useStyles } from './styles';
import {OUTCOME_STATUS, STATUS} from "@app/store/models/Order";
import Button from "@material-ui/core/Button";

const Content = observer(() => {
  const { orders } = useStore();

  const onReset = () => {
    orders.resetFilter();
  };

  const onExport = () => {
    orders.export();
  }

  return (
    <Grid container spacing={3} alignContent={'center'}>
      <Grid item xs={6} sm={3} lg={2}>
        <TapTextField
          debounce
          label={'search'}
          name={'search'}
          value={orders.filter.query}
          onChange={(v) => {
            orders.changeFilter('search', v);
          }}
        />
      </Grid>
      <Grid item xs={6} sm={3} lg={2}>
        <TapSelect
          label={'source'}
          value={orders.filter.source}
          options={['katy', 'artur', 'smooth_katy', 'clever_pips', 'all'].map(s => ({
            label: s, value: s === 'all' ? null : s
          }))}
          onChange={(v) => {
            orders.changeFilter('source', v);
          }}
        />
      </Grid>
      <Grid item xs={6} sm={2} lg={2}>
        <TapSelect
          label={'signal status'}
          value={orders.filter.status}
          options={[...STATUS, 'all'].map(s => ({
            label: s, value: s === 'all' ? null : s
          }))}
          onChange={(v) => {
            orders.changeFilter('status', v);
          }}
        />
      </Grid>
      <Grid item xs={6} sm={2} lg={2}>
        <TapSelect
          label={'outcome status'}
          value={orders.filter.source}
          options={[...OUTCOME_STATUS, 'all'].map(s => ({
            label: s, value: s === 'all' ? null : s
          }))}
          onChange={(v) => {
            orders.changeFilter('outcome_status', v);
          }}
        />
      </Grid>
      <Grid item xs={6} sm={3} lg={1}>
        <Button fullWidth variant='outlined' onClick={onReset} size={'large'}>
          reset
        </Button>
      </Grid>
      <Grid item xs={6} sm={3} lg={1}>
        <Button fullWidth color={'primary'} variant='outlined' onClick={onExport} size={'large'}>
          export
        </Button>
      </Grid>
    </Grid>
  )
});

export const OrderFilters = observer(() => {
  const classes = useStyles();

  return (
    <>
      <Hidden mdUp>
        <Accordion classes={{root: classes.root}}>
          <AccordionSummary
            classes={{root: classes.summary}}
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography variant={'h6'}>Filters</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Content />
          </AccordionDetails>
        </Accordion>
      </Hidden>
      <Hidden smDown>
        <Content />
      </Hidden>
    </>
  )
})
