import React from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import {truncate} from 'lodash-es';

export const Truncated = ({ title, variant = 'body1', size = 50 }) => {
  if (!title) return <></>

  if (title.length > size) {
    return (
      <Tooltip title={title}>
        <Typography variant={'caption'}>
          { truncate(title, {length: size, omission: '...'}) }
        </Typography>
      </Tooltip>
    )
  } else {
    return (
      <Typography variant={'caption'}>
        { title }
      </Typography>
    )
  }
}
