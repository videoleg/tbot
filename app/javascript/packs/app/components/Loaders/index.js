import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';

export const SkeletonTable = () => {
  return (
    <div>
      <Skeleton height={50} width='75%' />
      {Array.from({ length: 10 }).map((item, index) => {
        return <Skeleton height={55} key={index} />;
      })}
    </div>
  );
};

export const SkeletonSpinner = ({ size = 30 }) => {
  return (
    <Box display='flex' alignItems='center' justifyContent='center'>
      <CircularProgress size={size} />
    </Box>
  );
};
