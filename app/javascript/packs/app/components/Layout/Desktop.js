import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import { Link } from '@app/components/Link';

export const Desktop = () => {
  return (
    <AppBar position='static' color={'primary'}>
      <Toolbar variant='dense'>
        <Grid container spacing={5}>
          <Grid item>
            <Link to={'/orders'}>
              Orders
            </Link>
          </Grid>
          <Grid item>
            <Link to={'/logs'}>
              Logs
            </Link>
          </Grid>
          <Grid item>
            <Link to={'/new_order'}>
              <Button variant={'contained'}>New Order</Button>
            </Link>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  )
}
