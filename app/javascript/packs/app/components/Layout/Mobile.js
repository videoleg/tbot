import React, { useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import { Link } from '@app/components/Link';
import IconButton from '@material-ui/core/IconButton';
import { useStyles } from './styles';
import MenuIcon from '@material-ui/icons/Menu';
import AddBoxIcon from '@material-ui/icons/AddBox';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';


const Menu = ({ toggle }) => {
  const classes = useStyles();

  return (
    <div
      className={classes.list}
      onClick={toggle(false)}
      onKeyDown={toggle( false)}
    >
      <List>
        <ListItem button>
          <Link to={'/orders'}>
            Orders
          </Link>
        </ListItem>
        <ListItem button>
          <Link to={'/logs'}>
            Logs
          </Link>
        </ListItem>
      </List>
    </div>
  )
};

export const Mobile = () => {
  const classes = useStyles();

  const [opened, setOpened] = useState(false);

  const toggleDrawer = (value) => (event) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setOpened(value);
  };

  return (
    <div className={classes.root}>
      <SwipeableDrawer
        anchor={'left'}
        open={opened}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
      >
        <Menu toggle={toggleDrawer}/>
      </SwipeableDrawer>
      <AppBar position="static">
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
          <div className={classes.space}>
          </div>
          <Link to={'/new_order'}>
            <IconButton aria-label="search" color='inherit'>
              <AddBoxIcon />
            </IconButton>
          </Link>

        </Toolbar>
      </AppBar>
    </div>
  )
}
