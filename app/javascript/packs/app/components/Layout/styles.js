import { makeStyles, createStyles } from '@material-ui/core';

export const useStyles = makeStyles(theme => {
  return createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    toolbar: {
      alignItems: 'flex-start',
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
    space: {
      flexGrow: 1,
    },
    list: {
      width: 200,
    },
  })
});
