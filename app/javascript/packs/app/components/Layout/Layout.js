import React from 'react';
import Hidden from '@material-ui/core/Hidden';
import { Desktop } from './Desktop';
import { Mobile } from './Mobile';
import Container from '@material-ui/core/Container';

export const Layout = ({children}) => {
  return (
    <>
      <Hidden mdUp>
        <Mobile/>
      </Hidden>
      <Hidden smDown>
        <Desktop />
      </Hidden>
      {
        children
      }
    </>
  )
}
