import React from 'react';
import {generatePath, useHistory} from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import LinkIcon from '@material-ui/icons/Link';
import Tooltip from '@material-ui/core/Tooltip';
import {Typography} from "@material-ui/core";
import { ROUTES } from '@app/Root';
import { Link } from "react-router-dom";

export const Wave = ({ wave }) => {
  const history = useHistory();

  return (
    <ListItem style={{paddingLeft: 0}}>
      <ListItemIcon style={{ minWidth: '30px' }}>
        {
          wave.isProcessed ? (
            <CheckCircleIcon size={'small'}/>
          ) : (
            <Tooltip title={wave.note} size={'small'}>
              <HighlightOffIcon/>
            </Tooltip>
          )
        }
      </ListItemIcon>
      <ListItemText>
        {
          wave.order ? (
            <Link to={generatePath(ROUTES.ORDER, { id: wave.order.id })}>
              <Typography variant={'caption'}>
                { wave.text }
              </Typography>
            </Link>

          ) : (
            <Typography variant={'caption'}>
              { wave.text }
            </Typography>
          )
        }
      </ListItemText>
    </ListItem>
  )
}
