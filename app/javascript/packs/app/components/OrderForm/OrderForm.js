import React from 'react';
import {Grid} from '@material-ui/core';
import {
  FormikExtend,
  FormikAutocomplete,
  FormikSubmit,
  FormikTextField
} from '../FormikControls';
import { currencies, stringToOption } from '@app/shared';
import { toJS } from 'mobx';
import { OUTCOME, OUTCOME_STATUS, SETUP } from '@app/store/models/Order';
import Typography from '@material-ui/core/Typography';

const initialValues = {
  currency: currencies[0],
  type: 'buy',
  amount: 1,
  source: 'artur',
  risk: 1
};


export const OrderForm = ({
  order,
  onSubmit
}) => {
  const orderJSON = order ? toJS(order) : {};
  const values = {...initialValues, ...orderJSON};

  const isClosed = order && order.isClosed;
  const isCreated = order && order.id;

  return (
    <FormikExtend
      enableReinitialize
      initialValues={values}
      onSubmit={onSubmit}
    >
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} lg={6}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography variant={'h5'}>
                Main
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikAutocomplete
                disabled={isCreated}
                label={'type'}
                options={['sell', 'buy'].map(c => ({
                  value: c,
                  label: c
                }))}
                name={'type'} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikTextField
                disabled={isCreated}
                label={'amount'}
                name={'amount'}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikAutocomplete
                disabled={isCreated}
                label={'currency'}
                options={currencies.map(c => ({
                  value: c,
                  label: c
                }))}
                name={'currency'} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikTextField
                disabled={isClosed}
                label={'when'}
                name={'start'}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikTextField
                disabled={isClosed}
                label={'stop loss'}
                name={'stop'}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikTextField
                disabled={isClosed}
                label={'take profit 1'}
                name={'exit'}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikTextField
                disabled={isClosed}
                label={'take profit 2'}
                name={'exit_1'}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikTextField
                disabled={isClosed}
                label={'take profit 3'}
                name={'exit_2'}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <FormikTextField
                multiline
                rows={4}
                label={'comment'}
                name={'comment'} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} lg={6}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography variant={'h5'}>
                Notes
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikAutocomplete
                multiple
                creatable
                options={SETUP.map(stringToOption)}
                label={'setup'}
                name={'setup'} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikTextField
                label={'risk'}
                name={'risk'} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikAutocomplete
                creatable
                options={OUTCOME.map(stringToOption)}
                label={'outcome'}
                helperText={'only numbers'}
                name={'outcome'} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormikAutocomplete
                options={OUTCOME_STATUS.map(stringToOption)}
                label={'outcome status'}
                name={'outcome_status'} />
            </Grid>
            <Grid item xs={12} sm={12}>
              <FormikTextField
                label={'screenshot URL'}
                name={'image'} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <FormikSubmit size={'large'}>
            Save
          </FormikSubmit>
        </Grid>
      </Grid>
    </FormikExtend>
  )
}
