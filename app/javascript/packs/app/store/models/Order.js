import { Model } from 'libx';
import { Wave } from './Wave';
import {computed, observable} from 'mobx';
import * as moment from 'moment';
import { dateFormat } from '@app/shared';

export class Order extends Model {
  @observable
  id;
  @observable
  type
  @observable
  currency
  @observable
  type
  @observable
  stop
  @observable
  start
  @observable
  exit
  @observable
  close
  @observable
  amount
  @observable
  message
  @observable
  timestamp
  @observable
  created_at
  @observable
  updated_at
  @observable
  source
  @observable
  status
  @observable
  waves
  @observable
  risk
  @observable
  setup
  @observable
  image
  @observable
  outcome
  @observable
  outcome_status

  parse(dto ) {
    Object.assign(this, {
      ...dto,
      waves: dto.waves.map(wave => new Wave(wave, { parse: true }))
    })
    return this;
  }

  @computed
  get isClosed () {
    return this.status == 'canceled';
  }

  @computed
  get updated () {
    if (this.timestamp) {
      return moment(this.timestamp * 1000).format(dateFormat);
    }
  }

  @computed
  get setups () {
    if (this.setup && this.setup.indexOf) { // hack to check that it's an array
      return this.setup.join(', ');
    }
  }
}


export const OUTCOME_STATUS = ['active', 'canceled', 'closed'];
export const OUTCOME = ['-1', '0', '0.5', '1'];
export const SETUP = ['C1', 'C2', 'W', 'D', '4H', 'Double'];
export const STATUS = ['active', 'canceled'];
export const SOURCE = ['artur', 'katy', 'smooth_katy', 'clever_pips'];
