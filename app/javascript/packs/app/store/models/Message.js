import { Model } from 'libx';
import { computed, observable } from 'mobx';
import { Wave } from './Wave';
import { dateFormat } from '@app/shared';
import * as moment from 'moment';

export class Message extends Model {
  @observable
  id;
  @observable
  text
  @observable
  timestamp
  @observable
  source
  @observable
  waves

  parse(dto ) {
    Object.assign(this, {
      ...dto,
      waves: dto.waves.map(wave => new Wave(wave, { parse: true }))
    })
    return this;
  }


  @computed
  get updated () {
    if (this.timestamp) {
      return moment(this.timestamp * 1000).format(dateFormat);
    }
  }
}
