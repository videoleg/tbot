import { Model } from 'libx';
import {computed, observable} from 'mobx';
import * as moment from 'moment';
import { Order } from './Order';
import { dateFormat } from '@app/shared';

export class Wave extends Model {
  @observable
  id;
  @observable
  text
  @observable
  status
  @observable
  order

  parse(dto ) {
    Object.assign(this, {
      ...dto,
      order: dto.order ? new Order(dto.order) : null
    });
    return this;
  }

  @computed
  get isProcessed() {
    return this.status == 'processed';
  }

  @computed
  get updated () {
    if (this.timestamp) {
      return moment(this.timestamp * 1000).format(dateFormat);
    }
  }
}
