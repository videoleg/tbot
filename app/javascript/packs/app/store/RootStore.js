import React, { useContext, createContext } from 'react';
import { Paginated } from '@app/store/shared/Paginated';
import {Order} from './models/Order';
import { API } from '@app/api';
import { action } from 'mobx';
import {Record} from "./shared/Record";
import { Message } from "./models/Message";

export const StoreContext = createContext({});
export const StoreProvider = ({
  children,
  rootStore
}) => {
  return (
    <StoreContext.Provider value={rootStore}>{children}</StoreContext.Provider>
  );
};

class OrdersStore extends Paginated(Order, API.orders.index) {
  filter = {
    status: null,
    source: 'artur',
    query: null
  }

  @action
  create(data) {
    return API.orders.create(data).then(dto => {
      return this.set(dto);
    })
  }


  export () {
    return API.orders.export({ filter: this.filter });
  }

  @action
  delete (id) {
    return API.orders.delete(id).then(() => {
      this.remove(id)
    })
  }
}

class OrderStore extends Record(Order, API.orders.show) {
  @action
  update(data) {
    return API.orders.update(this.item.id, data).then(dto => {
      return this.set(dto);
    })
  }

  @action
  cancel() {
    return API.orders.cancel(this.item.id).then(dto => {
      return this.set(dto);
    })
  }
}

class MessagesStore extends Paginated(Message, API.messages.index) {

}


export class RootStore  {
  orders = new OrdersStore();
  order = new OrderStore();
  messages = new MessagesStore();
}

export const useStore = () => useContext(StoreContext);
