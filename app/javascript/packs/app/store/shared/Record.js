import { action, computed, observable, runInAction } from 'mobx';
import { model as LibxModel } from 'libx';

export function Record(model, cb) {
  class RecordStore {

    @observable
    item

    @observable
    loading

    @action
    load(id) {
      this.loading = true;
      this.item = null;

      return cb(id).then(res => {
        const item = res;

        runInAction(() => {
          this.set(item);
        });
      })
        .catch(err => {})
        .finally(() => {
          runInAction(() => {
            this.loading = false;
          })
        })
    }

    @action
    set(item) {
      this.item = LibxModel(new model())
        .set(item, {
          parse: true
        });
      return this.item;
    }

    @action
    reset() {
      this.item = null;
      this.loading = false;
    }
  }

  return RecordStore;
}
