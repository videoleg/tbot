import { action, computed, observable, runInAction } from 'mobx';
import { Collection } from './Collection';

export const DEFAULT_PAGE_NUMBER = 1;
export const DEFAULT_PAGE_SIZE = 10;
export const DEFAULT_SORTING = { id: true };
export const DEFAULT_PAGE = {
  size: DEFAULT_PAGE_SIZE,
  number: DEFAULT_PAGE_NUMBER
};

export function Paginated(
  model,
  load,
  options= {}
) {
  class PaginatedClass extends Collection(model) {
    @observable page = { ...DEFAULT_PAGE, ...options.page };
    @observable filter = { ...options.filter };
    @observable sort  = { ...DEFAULT_SORTING, ...options.sort };
    @observable total  = 0;
    @observable loading  = false;
    @observable loaded  = false;

    @action
    add(dto)  {
      dto.isNew = true;
      return runInAction(() => {
        const record = this.records.set(dto);
        if (this.total) {
          this.total += 1;
        }
        return record;
      });
    }

    @computed
    get pageSize() {
      return this.page.size;
    }

    @action
    setOptions(opts) {
      if (opts.filter) {
        this.changeFilters(opts.filter);
      }
      if (opts.sort) {
        this.sort = opts.sort;
      }
      if (opts.pageSize) {
        this.setPageSize(opts.pageSize);
      }
      if (opts.page) {
        this.page = opts.page;
      }
    }

    @action
    changeFilter(name, value) {
      this.filter = { ...this.filter, [name]: value };
      this.resetPage();
    }

    @action
    changeFilters(values) {
      this.filter = values;
      this.resetPage();
    }

    @action
    resetFilterField(key) {
      const { [key]: value, ...other } = this.filter;
      this.filter = { ...other };
    }

    @action
    changeSort(name, value) {
      if (value === null) {
        this.sort = DEFAULT_SORTING;
      } else {
        this.sort = { [name]: value };
      }
    }

    @action
    resetFilter() {
      this.filter = {};
      this.resetPage();
    }

    @action
    resetSort(value) {
      if (value) {
        this.sort = value;
      } else {
        this.sort = DEFAULT_SORTING;
      }
    }

    @action
    setPage(number) {
      this.page = {
        ...this.page,
        number
      };
    }

    @action
    setPageSize(size) {
      this.page = {
        size,
        number: DEFAULT_PAGE_NUMBER
      };
    }

    @action
    resetPage() {
      this.page = DEFAULT_PAGE;
    }

    @action
    load(params = {}) {
      const { filter, sort, page } = params;

      this.loading = true;
      this.loaded = false;

      return load({
        page: { size: this.pageSize, number: this.page.number, ...page },
        filter: { ...this.filter, ...filter },
        sort: { ...this.sort, ...sort }
      })
        .then(this.loadSuccess)
        .finally(() => {
          runInAction(() => {
            this.loading = false;
            this.loaded = true;
          });
        });
    }

    @action.bound
    loadSuccess(res) {
      this.deleteAll();
      this.setMany(res.data);
      this.total = res.meta.total || res.data.length;
      return this.all;
    }
  }

  return PaginatedClass;
}
