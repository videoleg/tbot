import { action, computed, observable } from 'mobx';
import { Store } from 'libx';
import { reduce } from 'lodash-es';

export function Collection(model) {
  class CollectionClass extends Store {
    constructor(options) {
      super({
        rootStore: null, // to allow root store to be null
        ...options
      });
    }

    @observable
    records = this.collection({
      model
    });

    @computed
    get all() {
      return (this.records.items || []);
    }

    @computed
    get indexed() {
      return reduce(
        this.records.items,
        (memo, item) => {
          memo[item.id] = item;
          return memo;
        },
        {}
      );
    }

    @action
    setMany(dtoArray) {
      return this.records.set(dtoArray);
    }

    @action
    deleteMany(entities) {
      for (const ent of entities) {
        this.remove(ent.id);
      }
      return this;
    }

    @action
    deleteAll() {
      return this.records.clear();
    }

    @action
    set(dto) {
      return this.records.set(dto);
    }

    @action
    remove(id) {
      return this.records.remove(id);
    }
  }

  return CollectionClass;
}
