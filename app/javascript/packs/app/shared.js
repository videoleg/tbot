export function resolveServerErrors(error) {
  const DEFAULT_MESSAGE = {
    errors: [{ detail: 'Unknown error' }]
  };

  const errorMap = {
    422: error.data,
    404: {
      errors: [{ detail: 'Object was not found' }]
    },
    403: {
      errors: [
        { detail: 'You do not have permission to perform this operation' }
      ]
    },
    500: {
      errors: [{ detail: 'Unable to execute command due to the server error' }]
    },
    504: {
      errors: [
        {
          detail:
            'Unable to execute command due to the server timeout. Try refreshing page or contacting support'
        }
      ]
    }
  };

  const { errors } = errorMap[error.status] || DEFAULT_MESSAGE;

  return errors;
}

export const enumToOption = (obj) => {
  return Object.values(obj).map(field => ({
    label: field,
    value: field
  }));
}

export const stringToOption = (field) => ({
  label: field,
  value: field
});

export const colors = {
  neutral1: '#404A65',
  neutral2: '#636F87',
  neutral3: '#808FAD',
  neutral4: '#B8C3DA',
  neutral5: '#DDDFE7',
  neutral6: '#EEF0F2',
  neutral7: '#F1F5FB',
  neutral8: '#F8F9FC',
  neutral9: '#FBFBFD',
  neutral10: '#FFFFFF',

  yellow0: '#B47A27',
  yellow05: '#E59000',
  yellow1: '#FD9F00',
  yellow2: '#FFE3B3',
  yellow3: '#FFF7E9',

  blue1: '#078AF2',
  blue2: '#2684FF',
  blue3: '#A4D4FC',
  blue4: '#C4E1F7',
  blue5: '#E3F2FD',
  blue6: '#F0F8FD',

  red1: '#EE433B',
  red3: '#FFE9E8',

  green0: '#347937',
  green1: '#4BAE4F',
  green2: '#E9F8E9'

};

export const currencies = gon.currencies;

export const dateFormat = 'H:mm DD.MM.YY';
