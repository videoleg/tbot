import {ADAPTER, prepareParams, processCollection, processRecord} from './index';

const URL = 'messages';

export const messages = {
  index: (params) => {
    return ADAPTER.get(URL, prepareParams(params))
      .then(processCollection);
  }
}
