import {ADAPTER, prepareParams, processCollection, processRecord} from './index';
import QS from 'qs';

const URL = 'orders';

export const orders = {
  index: (params) => {
    return ADAPTER.get(URL, prepareParams(params))
      .then(processCollection);
  },
  create: (data) => {
    return ADAPTER.post(URL, { data: data })
      .then(processRecord);
  },
  update: (id, data) => {
    return ADAPTER.put(`${URL}/${id}`, { data: data })
      .then(processRecord);
  },
  show: (id) => {
    return ADAPTER.get(`${URL}/${id}`)
      .then(processRecord);
  },
  cancel: (id) => {
    return ADAPTER.put(`${URL}/${id}/close`)
      .then(processRecord);
  },
  delete: (id) => {
    return ADAPTER.delete(`${URL}/${id}`);
  },
  export: (data) => {
    const params = prepareParams({...data, sort: { id: true }}).params;
    params.page = { number: 1, size: 1000000 };
    const query = QS.stringify(params, { arrayFormat: 'brackets' });
    window.open(`${URL}/export?${query}`);
  }
}
