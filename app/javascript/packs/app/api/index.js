import axios, { AxiosRequestConfig } from 'axios';
import {compact, isEmpty} from 'lodash-es';
import QS from 'qs';
import { orders } from './orders';
import { messages } from './messages';

const token = document
  .querySelector("meta[name='csrf-token']")
  ?.getAttribute('content');

const common = {
  ['X-Requested-With']: 'XMLHttpRequest',
  'X-CSRF-TOKEN': token
};

const apiHeaders = {
  headers: {
    common
  }
};

const API_CONFIG = {
  baseURL: `/api/v2`,
  ...apiHeaders
};

export const prepareParams = (params) => {
  const { sort, page, filter } = params;

  const options = {
    page
  };

  // sorting
  if (sort && !isEmpty(sort)) {
    const key = Object.keys(sort)[0];
    options['sort'] = sort[key] ? `-${key}` : key;
  }

  // filtering
  if (filter && !isEmpty(filter)) {
    options.filter = compact(Object.keys(filter).map(key => {
      if (!isEmpty(filter[key])) {
        return {
          type: 'eq',
          key: key,
          value: filter[key]
        }
      }
    }));
  }

  return {
    params: options,
    paramsSerializer: (params) => QS.stringify(params, { arrayFormat: 'brackets' })
  };
}

export const processCollection = (res) => {
  const { data, meta } = res.data;
  return {
    data: data.map(processRecord),
    meta: meta
  }
}

export const processRecord = (dto) => {
  if (dto.data) {
    return processRecord(dto.data);
  } else {
    const { id, attributes } = dto;
    return {
      ...attributes, id
    };
  }
}

export const ADAPTER = axios.create(API_CONFIG);

export const API = {
  orders,
  messages
};


