class Order < ApplicationRecord
  include SpreadsheetArchitect

  self.inheritance_column = nil

  enum type: { buy: 0, sell: 1 }
  enum source: { katy: 0, artur: 1, smooth_katy: 2, clever_pips: 3 }
  enum status: { active: 0, canceled: 1 }
  enum outcome_status: { active: 0, canceled: 1, closed: 2 }, _prefix: true

  has_many :waves, class_name: 'Wave'

  scope :by_search, ->(query) { where('currency ilike ?', "%#{query}%") }
  scope :by_period, ->(days) { where('timestamp > ?', days.to_i.days.ago.to_i) }

  def to_text
    "#{type} #{currency.upcase}; stop: #{stop}; take: #{exit}; close: #{close}"
  end

  def spreadsheet_columns
    [
      ["id", :id],
      ["currency", :currency],
      ["type", :type],
      ["stop", :stop],
      ["start", :start],
      ["exit", :exit],
      ["close", :close],
      ["amount", :amount],
      ["message", :message],
      ["timestamp", (Time.at(timestamp).strftime("%d.%m.%Y %H:%M") rescue nil)],
      ["source", :source],
      ["status", :status],
      ["comment", :comment],
      ["risk", :risk],
      ["setup", :setup],
      ["image", :image],
      ["outcome", :outcome],
      ["outcome_status", :outcome_status],
      ["created_at", created_at.strftime("%d.%m.%Y %H:%M")],
      ["updated_at", updated_at.strftime("%d.%m.%Y %H:%M")],
    ]
  end
end
