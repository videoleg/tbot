class Message < ApplicationRecord
  DIGIT_REGEX = /([1-9]\d*|0)(\.\d+)/
  NEW_ORDER_REGEX = /(buy|sell)( )(\w{6})( )(#{DIGIT_REGEX})( )(stop|exit)( )(#{DIGIT_REGEX})( )(stop|exit)( )(#{DIGIT_REGEX})/
  CORRECTION_REGEX = /(close)( )(\w{6})/
  DIGIT_FIX_REGEX = /( )([.](?:0|[1-9]\d*)(?:\.(?:\d*[1-9]|0))?)/
  UNIT_REGEX = /#{DIGIT_REGEX}( )(unit|units)/
  SPLIT_WORDS = %w(sell buy close levels) # might be multiple signals in one message
  WHITE_LIST_WORDS = %w(stop exit) + SPLIT_WORDS

  has_many :waves, class_name: "Wave"
  has_many :orders, through: :waves

  enum source: { katy: 0, smooth_katy: 1, clever_pips: 2 }
end
