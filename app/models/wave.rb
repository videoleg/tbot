class Wave < ApplicationRecord
  belongs_to :order, optional: true
  belongs_to :message

  enum status: { unrecognized: 0, processed: 1, failed: 2 }
end