class Order::Operation::Close < Trailblazer::Operation
  pass :setup_params!
  pass Model(Order, :find)
  step Contract::Build(constant: Order::Contract::Close)
  step Contract::Validate()
  step Contract::Persist()
  pass :notify!

  def setup_params!(_input, params:, **)
    params[:timestamp] = Time.zone.now.to_i
    params[:status] = 'canceled'
    params[:outcome_status] = 'canceled'
    params[:outcome] = 0
  end

  def notify!(_input, model:, **)
    Notify::Order::Cancel.call(model)
  end
end
