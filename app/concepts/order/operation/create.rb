class Order::Operation::Create < Trailblazer::Operation
  pass :setup_params!
  pass Model(Order, :new)
  step Contract::Build(constant: Order::Contract::Create)
  step Contract::Validate()
  step Contract::Persist()
  pass :notify!

  def setup_params!(_input, params:, **)
    params[:timestamp] = Time.zone.now.to_i
    params[:status] = :active
  end

  def notify!(_input, model:, **)
    Notify::Order::Create.call(model)
  end
end
