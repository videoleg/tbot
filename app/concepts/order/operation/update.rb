class Order::Operation::Update < Trailblazer::Operation
  pass :setup_params!
  step Model(Order, :find)
  step Contract::Build(constant: Order::Contract::Update)
  step Contract::Validate()
  step Contract::Persist()
  pass :notify!

  def setup_params!(_input, params:, **)
    params[:timestamp] = Time.zone.now.to_i
  end

  def notify!(input, model:, **)
    changed = input['contract.default'].changed.select {|_, v| v}.keys
    changes = model.attributes.slice(*changed)
    Notify::Order::Update.call(model, changes)
  end
end
