module Order::Contract
  module Component::Base
    include Reform::Form::Module

    property :stop
    property :start
    property :exit
    property :exit_1
    property :exit_2
    property :close
    property :timestamp
    property :comment
    property :risk
    property :setup
    property :image
    property :outcome
    property :outcome_status
    property :message

    validation name: :base do
      params do
        required(:stop).filled
        required(:start).filled
        required(:exit).filled
        required(:timestamp).filled
        optional(:close)
        optional(:message)
        optional(:comment)
        optional(:risk)
        optional(:setup)
        optional(:image)
        optional(:outcome)
        optional(:outcome_status)
        optional(:exit_1)
        optional(:exit_2)
      end
    end
  end
end
