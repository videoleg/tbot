module Order::Contract
  class Close < Reform::Form
    property :status
    property :close
    property :outcome_status
    property :outcome
    property :wafe_ids

    validation do
      params do
        required(:status).value(eql?: 'canceled')
        required(:outcome_status).value(eql?: 'canceled')
      end
    end
  end
end
