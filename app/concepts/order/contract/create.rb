module Order::Contract
  class Create < Reform::Form
    include Component::Base

    property :currency, populator: ->(fragment:, **) { self.currency = Currency[fragment] }
    property :type
    property :source
    property :amount
    property :status
    property :wafe_ids

    validation do
      params do
        required(:currency).value(included_in?: Currency.all)
        required(:source).filled(included_in?: Order.sources.keys)
        required(:type).filled(included_in?: Order.types.keys)
        required(:status)
        required(:amount).filled
        required(:timestamp).filled
      end
    end
  end
end
