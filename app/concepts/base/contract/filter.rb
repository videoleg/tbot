require 'disposable/twin/property/hash'

module Base::Contract
  class Filter < Reform::Form
    feature Disposable::Twin::Property::Hash

    collection :filter, virtual: true, twin: Filter::Twin, populator: :populate_filter!

    def populate_filter!(index:, collection:, fragment: {}, **)
      collection[index] = Filter::Twin.new(OpenStruct.new(model_class: model, **fragment))
    end
  end
end

# Example of filter value:
#
# {
#   filter: [{
#   type: "in",
#   value: ["33930be0-92cb-40e6-82ba-9611db4c3cb4"],
#   key: "id",
#   or: [{
#     type: "eq",
#     value: "1a818ae0-6a97-4496-a05d-32a7ca76c3e9",
#     key: "id"
#   }, {
#     type: "in",
#     not: true,
#     value: ["pending"],
#     key: "status"
#   }]
# }]
# }
