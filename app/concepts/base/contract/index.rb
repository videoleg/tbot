require 'disposable/twin/property/hash'
require 'reform/form/coercion'

module Base::Contract
  class Index < Reform::Form
    DEFAULT_SORT_BY = %w[created_at].freeze

    feature Disposable::Twin::Property::Hash
    feature Coercion

    property :page, virtual: true, field: :hash, populate_if_empty: Hash do
      property :size, default: 10
      property :number, default: 1

      validation do
        params do
          required(:size).value(:integer, gteq?: 1)
          required(:number).value(:integer, included_in?: (1..10000))
        end
      end
    end
    property :sort, virtual: true, populator: :populate_sort!, default: 'created_at'
    property :direction, as: :sort, virtual: true, populator: :populate_direction!, default: 'asc'

    def populate_direction!(fragment:, **)
      self.direction = fragment.include?('-') ? 'desc' : 'asc'
    end

    def populate_sort!(fragment:, **)
      self.sort = fragment.delete('-')
    end

    def model_attributes
      model.new.attributes.keys
    end

    validation contract: AppContract do
      option :form

      params do
        required(:page).value(:hash)
        required(:direction).value(:string)
        required(:sort).value(:string)
      end

      # rule(:sort) do
      #   key.failure(I18n.t('dry_validation.errors.sort?.arg.default', sort: model_attributes)) if form.model_attributes.exclude?(value)
      # end
    end
  end
end
