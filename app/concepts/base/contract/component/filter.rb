require 'disposable/twin/property/hash'
require 'reform/form/coercion'

module Base::Contract
  module Component::Filter
    include Reform::Form::Module

    feature Reform::Form::Coercion
    feature Disposable::Twin::Property::Hash

    property :model_class # ActiveRecord class
    property :type
    property :key
    property :not
    property :value, populator: ::Base::Populator::Filter::Value

    validation contract: AppContract do
      option :form

      params do
        required(:type).value(included_in?: FILTER_TYPE.values)
        required(:key).filled
        optional(:value)
      end

      rule(:key) do
        key.failure(:invalid_filter) unless form.scope? || form.attribute?
      end

      rule(:type) do
        key.failure(:invalid_filter) unless form.scope? || form.valid_types.include?(form.type)
      end

      rule(:value) do
        key.failure(:invalid_filter) unless value.present? || form.allow_empty_value?
      end
    end

    def attribute?
      model_class.new.attributes.key?(key)
    end

    def scope?
      model_class.respond_to?(scope_name)
    end

    def allow_empty_value?
      type == FILTER_TYPE[:in] || column_type == :boolean
    end

    def scope_name
      "by_#{key}"
    end

    def column_type
      model_class.columns_hash[key]&.type
    end

    def valid_types
      case column_type
      when :uuid, :id then FILTER_TYPE.slice(:eq, :in)
      when :string, :text then FILTER_TYPE.slice(:eq, :contains, :in)
      when :integer, :float, :decimal, :datetime, :date then FILTER_TYPE.slice(:eq, :lte, :gte, :lt, :gt, :in)
      when :boolean then FILTER_TYPE.slice(:eq)
      else {}
      end.values
    end
  end
end
