module Base::Contract
  class Filter::Twin < Reform::Form
    include Component::Filter

    collection :or, field: :hash, virtual: true, populator: :populate_or! do # recursive "or" conditions
      include Component::Filter
    end

    def populate_or!(fragment: {}, **)
      self.or.append fragment.merge(model_class: model_class)
    end
  end
end
