module Base::Populator
  class Filter::Value
    class << self
      def call(contract, as:, fragment:, **)
        contract.public_send "#{as}=", case contract.column_type
                                       when :date then parse_date(fragment)
                                       when :datetime then parse_datetime(fragment)
                                       when :boolean then fragment.to_s == 'true'
                                       else fragment
                                       end
      end

      def parse_date(value)
        Date.parse(value)
      rescue TypeError, Date::Error
        nil
      end

      def parse_datetime(value)
        DateTime.parse(value)
      rescue TypeError, Date::Error
        nil
      end
    end
  end
end
