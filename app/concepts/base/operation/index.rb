class Base::Operation::Index < Trailblazer::Operation
  pass :model!
  step Subprocess(Base::Operation::Filter), output: :filter_output
  step Contract::Build(constant: Base::Contract::Index)
  step Contract::Validate()
  pass :sort!
  pass :paginate!

  private

  def model!(input, model:, **)
    input[:model] = model.all
  end

  def filter_output(_input, **options)
    {
      model: options[:model],
      errors: options[:'contract.default'].errors.messages
    }
  end

  def sort!(input, model:, **)
    key = input['contract.default'].sort
    direction = input['contract.default'].direction
    input[:model] = model.order(key => direction)
  end

  def paginate!(input, model:, **)
    page = input['contract.default'].page.number
    size = input['contract.default'].page.size
    input[:model] = model.page(page).per(size)
  end
end
