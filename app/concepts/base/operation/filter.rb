class Base::Operation::Filter < Trailblazer::Operation
  step Contract::Build(constant: Base::Contract::Filter)
  step Contract::Validate()
  pass :filter!

  private

  def filter!(input, model:, **)
    input[:model] = input['contract.default'].filter.inject(model) do |scope, filter|
      Filter.new(scope, filter).call
    end
  end
end

class Filter
  attr_reader :scope, :conditions, :filter

  SELECTOR_ARGS_MAP = {
    eq: ->(k, v) { { k => v } },
    in: ->(k, v) { { k => v } },
    lte: ->(k, v) { ["#{k} <= ?", v] },
    lt: ->(k, v) { ["#{k} < ?", v] },
    gt: ->(k, v) { ["#{k} > ?", v] },
    gte: ->(k, v) { ["#{k} >= ?", v] },
    contains: ->(k, v) { ["#{k} ilike ?", "%#{v}%"] }
  }.freeze

  def initialize(scope, filter)
    @scope = scope
    @filter = filter
    @conditions = filter.try(:or)&.map do |value|
      Filter.new(scope, value)
    end
  end

  def call
    if filter.scope? # fallback to model scope
      apply_scope_filter
    else
      apply_column_filter
    end
    apply_or_filter
    scope
  end

  private

  def apply_scope_filter
    @scope = scope.public_send(filter.scope_name, filter.value)
  end

  def apply_column_filter
    scope_args = SELECTOR_ARGS_MAP[filter.type.to_sym].call(filter.key, filter.value)
    @scope = selector.call(scope_args)
  end

  def apply_or_filter
    return unless conditions&.any?

    @scope = conditions.inject(scope) do |result, condition|
      result.or(condition.call)
    end
  end

  def selector
    lambda do |*args|
      filter.not ? scope.where.not(*args) : scope.where(*args)
    end
  end
end
