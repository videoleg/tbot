class Base::Operation::Show < Trailblazer::Operation
  step :model!

  private

  def model!(input, params:, model:, **)
    input[:model] = model.find_by(id: params[:id])
  end
end
