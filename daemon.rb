ENV['RAILS_ENV'] ||= 'development'
dir = File.dirname(__FILE__)

require dir + "/config/environment"

Daemons.run_proc('tg_daemon.rb') do
  Bot.listen
end