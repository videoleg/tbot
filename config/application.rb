require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Tbot
  class Application < Rails::Application
    config.load_defaults 6.0
    config.secret_key_base = ENV['SECRET_KEY_BASE']
    config.autoload_paths << "#{config.root}/lib"
    config.time_zone = 'Moscow'
    config.autoloader = :classic
  end
end
