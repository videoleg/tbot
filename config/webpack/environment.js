const { environment } = require('@rails/webpacker')

const path = require('path')

module.exports = environment


const customConfig = {
  resolve: {
    alias: {
      '@app': path.resolve(__dirname, '..', '..', 'app/javascript/packs/app'),
    }
  }
}

environment.config.merge(customConfig)
