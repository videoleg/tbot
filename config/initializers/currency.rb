class Currency
  class << self
    def all
      CURRENCIES
    end

    def [](query)
      CURRENCIES.find do |c|
        c.downcase == query.to_s.downcase
      end
    end
  end
end
