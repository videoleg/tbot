CURRENCIES = %w[
  GBPUSD
  AUDUSD
  NZDUSD
  USDCHF
  USDCAD
  USDJPY
  EURGBP
  EURAUD
  EURUSD
  EURNZD
  EURCHF
  EURCAD
  EURJPY
  GBPAUD
  GBPNZD
  GBPCHF
  GBPCAD
  GBPJPY
  AUDNZD
  AUDCHF
  AUDCAD
  AUDJPY
  NZDCHF
  NZDCAD
  NZDJPY
  CHFJPY
  CADJPY
  CADCHF
  WTI
  BTCUSD
  ETHUSD
  XAUUSD
  XAGUSD
  .US500Cash
].freeze

FILTER_TYPE = {
  eq: 'eq',
  lte: 'lte',
  lt: 'lt',
  gte: 'gte',
  gt: 'gt',
  in: 'in',
  contains: 'contains'
}.freeze
