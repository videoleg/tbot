# Setup Reform
require 'reform/form/dry'

Reform::Form.class_eval do
  include Reform::Form::Dry
end

if Rails.env.test? || Rails.env.local?
  require 'trailblazer/developer'
  TrbDev = Trailblazer::Developer
end
