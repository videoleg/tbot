Rails.application.routes.draw do
  root "orders#index"

  resources :orders, only: [:index] do
    get :export, on: :collection
  end
  namespace :api do
    # v1 external
    namespace :v1 do
      resources :orders
    end

    # v2 internal
    namespace :v2 do
      resources :orders do
        put :close, on: :member
      end
      resources :messages
    end
  end
end
