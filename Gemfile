source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

gem 'rails', '~> 6.0.3', '>= 6.0.3.4'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 4.1'
gem 'sass-rails', '>= 6'
gem 'turbolinks', '~> 5'
gem 'jquery-rails'
gem 'rack-cors', :require => 'rack/cors'
gem 'dotenv-rails', require: 'dotenv/rails-now'
gem 'haml-rails', '~> 2.0'
gem 'kaminari'
gem 'bootstrap', '~> 5.0.0.alpha3'
gem 'whenever', require: false
gem 'devise'
gem 'daemons'
gem 'webpacker'
gem 'trailblazer'
gem 'reform', '~> 2.3.2'
gem 'jsonapi-serializer'
gem 'dry-matcher'
gem 'dry-monads'
gem 'dry-validation', '~> 1.4.2'
gem 'telegram-bot'
gem 'gon'
gem 'spreadsheet_architect'
gem 'tdlib-ruby', '~> 2.2.0'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'pry'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Strategies for cleaning databases. Can be used to ensure a clean slate for testing. (https://github.com/DatabaseCleaner/database_cleaner)
  gem 'database_cleaner'

  # A gem providing 'time travel' and 'time freezing' capabilities, making it dead simple to test time-dependent code.  It provides a unified method to mock Time.now, Date.today, and DateTime.now in a single call. (https://github.com/travisjeffery/timecop)
  gem 'timecop'

  # RSpec for Rails (https://github.com/rspec/rspec-rails)
  gem 'rspec-rails'

  # RSpec JUnit XML formatter (http://github.com/sj26/rspec_junit_formatter)
  gem 'rspec_junit_formatter'

  # rspec command for spring (https://github.com/jonleighton/spring-commands-rspec)
  gem 'spring-commands-rspec'

  # Simple one-liner tests for common Rails functionality (https://matchers.shoulda.io/)
  gem 'shoulda-matchers'

  # Record your test suite's HTTP interactions and replay them during future test runs for fast, deterministic, accurate tests. (https://relishapp.com/vcr/vcr/docs)
  gem 'vcr'

  # Library for stubbing HTTP requests in Ruby. (http://github.com/bblimke/webmock)
  gem 'webmock'

  # factory_bot_rails provides integration between factory_bot and rails 5.0 or newer (https://github.com/thoughtbot/factory_bot_rails)
  gem 'factory_bot_rails'

  # Easily generate fake data (https://github.com/faker-ruby/faker)
  gem 'faker'
end



# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :production do
  gem 'unicorn', '5.4.1'
end
