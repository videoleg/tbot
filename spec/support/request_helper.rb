module RequestHelper
  def response_json
    @response_json ||= ActiveSupport::JSON.decode response.body
  end
end
