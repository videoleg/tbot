require 'rails_helper'
require 'shared_contexts/request'
require 'shared_examples/request'

RSpec.describe 'GET /api/v2/orders' do
  let(:order_1) { create :order, source: :katy }
  let(:order_2) { create :order, source: :artur }

  include_context 'with request action' do
    let(:path) { '/api/v2/orders' }
    let(:method) { :get }
  end

  context 'when success' do
    before do
      order_1
      order_2
    end

    it do
      expect(subject).to eq 200
      expect(response_json['data'].size).to eq 2
    end

    context 'when with filter' do
      let(:params) do
        {
          filter: [{
                     type: FILTER_TYPE[:eq],
                     value: 'katy',
                     key: 'source'
                  }]
        }
      end

      it do
        expect(subject).to eq 200
        expect(response_json['data'].size).to eq 1
      end
    end
  end
end
