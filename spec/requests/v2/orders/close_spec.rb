require 'rails_helper'
require 'shared_contexts/request'
require 'shared_examples/request'

RSpec.describe 'PUT /api/v2/orders/:ID/close' do
  let(:model) { create :order }

  include_context 'with request action' do
    let(:path) { "/api/v2/orders/#{model.id}/close" }
    let(:method) { :put }
    let(:params) do
      {
        data: {
          close: 3
        }
      }
    end
  end

  context 'when success' do
    it do
      expect(subject).to eq 200
      expect(response_json).to include(
                                 'data' => a_hash_including(
                                   'id' => an_instance_of(String),
                                   'attributes' => a_hash_including(
                                     'status' => 'canceled',
                                     'close' => 3,
                                     'timestamp' => an_instance_of(Integer),
                                     'outcome_status' => 'canceled',
                                     'outcome' => 0
                                   ),
                                 )
                               )
    end
  end
end
