require 'rails_helper'
require 'shared_contexts/request'
require 'shared_examples/request'

RSpec.describe 'POST /api/v2/orders' do
  include_context 'with request action' do
    let(:path) { '/api/v2/orders' }
    let(:method) { :post }
    let(:params) do
      {
        data: {
          amount: 1,
          source: :katy,
          currency: 'eurusd',
          type: 'buy',
          start: 1.001,
          exit: 1.002,
          stop: 1.003
        }
      }
    end
  end

  context 'when success' do
    it do
      expect(subject).to eq 200
      expect(response_json).to include(
        'data' => a_hash_including(
          'id' => an_instance_of(String),
          'attributes' => a_hash_including(
            'amount' => 1,
            'currency' => 'EURUSD',
            'type' => 'buy',
            'start' => 1.001,
            'exit' => 1.002,
            'stop' => 1.003,
            'status' => 'active',
            'source' => 'katy',
            'timestamp' => an_instance_of(Integer)
          ),
        )
      )
    end
  end
end
