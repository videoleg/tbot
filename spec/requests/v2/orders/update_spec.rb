require 'rails_helper'
require 'shared_contexts/request'
require 'shared_examples/request'

RSpec.describe 'PUT /api/v2/orders/:ID' do
  let(:model) { create :order }

  include_context 'with request action' do
    let(:path) { "/api/v2/orders/#{model.id}" }
    let(:method) { :put }
    let(:params) do
      {
        data: {
          start: 2,
          exit: 2,
          stop: 2
        }
      }
    end
  end

  context 'when success' do
    it do
      expect(subject).to eq 200
      expect(response_json).to include(
                                 'data' => a_hash_including(
                                   'id' => an_instance_of(String),
                                   'attributes' => a_hash_including(
                                     'start' => 2,
                                     'exit' => 2,
                                     'stop' => 2,
                                     'timestamp' => an_instance_of(Integer)
                                   ),
                                   )
                               )
    end
  end
end
