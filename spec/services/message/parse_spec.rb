require 'rails_helper'
require 'shared_contexts/request'
require 'shared_examples/request'

RSpec.describe 'Message::Parse' do

  let(:currency) { 'CADCHF' }
  let(:text) do
    %Q(
      Buy - #{currency}  1.97458
      TP1: 1.97658
      TP2: 1.97958
      TP3: 1.98458
      SL  : 1.97158
    )
  end
  let(:message) { create :message, text: text }
  subject(:result) { Message::Parse.call(message) }

  context 'when new order' do
    it do
      subject
      expect(Order.last.currency).to eq 'CADCHF'
      expect(Order.last.type).to eq 'buy'
      expect(Order.last.start).to eq 1.97458
      expect(Order.last.stop).to eq 1.97158
      expect(Order.last.exit).to eq 1.97658
      expect(Order.last.exit_1).to eq 1.97958
      expect(Order.last.exit_2).to eq 1.98458
      expect(Order.last.status).to eq 'active'
    end
    it { expect { subject }.to change(Order, :count).by(1) }
    it { expect { subject }.to change(Wave, :count).by(1) }
  end

  context 'when close' do
    let(:close_text) { "Close #{currency} long at .6954" }
    let(:message) { create :message, text: close_text }

    context 'when failed' do
      it do
        subject
        expect(Wave.last.status).to eq 'failed'
      end
      it { expect { subject }.to change(Order, :count).by(0) }
      it { expect { subject }.to change(Wave, :count).by(1) }
    end
  end
end
