RSpec.shared_examples 'request not found' do
  it do
    expect(subject).to eq 404
    expect(response_json).to include(
                               'message' => I18n.t('errors.not_found')
                             )
  end
end

RSpec.shared_examples 'request unauthorized' do
  it do
    expect(subject).to be_unauthorized
    expect(subject.body).to eq ''
  end
end

RSpec.shared_examples 'request unprocessable entity' do
  it do
    expect(subject).to eq 422
    expect(response_json['errors']).to include(
                                         a_hash_including('detail', 'source', 'status', 'title')
                                       )
  end
end

RSpec.shared_examples 'request no data' do
  it do
    expect(subject).to eq 200
    expect(response_json['data']).to be_empty
  end
end

# pagination related:

RSpec.shared_context 'when request pagination context' do
  let(:record) {}
  let(:type) {}
  let(:sort) {}
  let(:filter) { {} }
  let(:direction) { '-' }
  let(:params) do
    super().merge \
      page: {
      size: 1,
      number: 2
    },
      sort: "#{direction}#{sort}",
      filter: filter
  end
end

RSpec.shared_examples 'request pagination' do
  include_context 'request pagination context'

  it do
    expect(subject).to be_successful
    expect(response_json).to include(
                               'data' => a_collection_including(
                                 'id' => record.id,
                                 'type' => type
                               ),
                               'links' => a_hash_including(
                                 'first' => a_string_matching(/page\[number\]=1/),
                                 'last' => a_string_matching(/page\[number\]=2/),
                                 'prev' => a_string_matching(/page\[number\]=1/),
                                 'next' => nil
                               )
                             )
  end
end

RSpec.shared_examples 'request pagination with relations' do
  it do
    expect(subject).to eq 200
    expect(response_json).to include(
                               'data' => a_collection_including(
                                 'id' => record.id,
                                 'type' => type,
                                 'attributes' => an_instance_of(Hash),
                                 'relationships' => an_instance_of(Hash)
                               ),
                               'links' => a_hash_including(
                                 'first' => a_string_matching(/page\[number\]=1/),
                                 'last' => a_string_matching(/page\[number\]=2/),
                                 'prev' => a_string_matching(/page\[number\]=1/),
                                 'next' => nil
                               ),
                               'meta' => a_hash_including(
                                 'current_page' => 2,
                                 'per_page' => 1,
                                 'total' => 2,
                                 'total_pages' => 2,
                                 'first_page' => 1,
                                 'last_page' => 2
                               )
                             )
  end
end
