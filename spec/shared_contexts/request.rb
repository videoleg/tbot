RSpec.shared_context 'with request action' do
  subject { action.call }

  let(:params) do
    {
      data: {
        attributes: attributes,
        relationships: relationships
      }
    }
  end
  let(:headers) {}
  let(:path) {}
  let(:method) { :get }
  let(:action) { -> { public_send(method, path, params: params, headers: headers) } }
  let(:attributes) {}
  let(:relationships) {}
end

RSpec.shared_context 'with request pagination' do
  let(:record) {}
  let(:type) {}
  let(:sort) { 'created_at' }
  let(:filter) { {} }
  let(:direction) { '-' }
  let(:size) { 10 }
  let(:number) { 1 }
  let(:params) do
    super().merge \
      page: {
      size: size,
      number: number
    },
      sort: "#{direction}#{sort}",
      filter: filter
  end
  let(:attributes) { {} }
end

RSpec.shared_context 'with request authorization' do
  # let(:token) { create :token, user: user }
  # let(:headers) do
  #   {
  #     'HTTP_AUTHORIZATION' => token[:access_token],
  #     'HTTP_IDENTIFICATION' => token[:id_token]
  #   }
  # end
end
