RSpec.shared_context 'with trailblazer' do
  subject { described_class.call operation_params }


  let(:operation_params) do
    {
      params: params
    }.merge(dependency_params)
  end
  let(:params) { {} }
  let(:dependency_params) do
    {
    }
  end
end
