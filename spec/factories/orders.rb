rand_price = lambda { rand(1000).to_f / 1000 }

FactoryBot.define do
  factory :order do
    type { :buy }
    currency { Currency.all.sample }
    stop { rand_price.call }
    start { rand_price.call }
    close { rand_price.call }
    exit { rand_price.call }
    amount { 1 }
    message { 'test' }
    timestamp { Time.zone.now.to_i }
  end
end
