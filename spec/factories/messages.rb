FactoryBot.define do
  factory :message do
    payload do
      { 'chat_id' => ENV['TG_SOURCE_1'] }
    end
  end
end
